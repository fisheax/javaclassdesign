package zust.java.lx.net.Lexer;

import java.util.ArrayList;
import java.util.logging.Logger;

import zust.java.lx.net.Log.HtmlLogger;
import zust.java.lx.net.TagNode.CommonTagNode;

/**
 * 
 * 本类提供层级的节点存储，不规定多少层，不够的话会自动调整
 * @author muxue
 *
 */

public class HtmlTagLevelStorage
{
	//层级存储
	private static ArrayList<ArrayList<CommonTagNode>> tagLevelStorage = new ArrayList<ArrayList<CommonTagNode>>();
	//日志记录
	private static Logger logger = HtmlLogger.getLogger();
	
	static
	{
		//默认先提供20层
		for (int i = 0; i < 20; i++)
		{
			tagLevelStorage.add(new ArrayList<CommonTagNode>());
		}
	}
	
	//获取某个层级的ArrayList
	public static ArrayList<CommonTagNode> getTagLevel(int level)
	{
		if (level < 0)
		{
			logger.severe("The index must be greater or equal than 0 ...");
			return null;
		}
		
		try
		{
			return tagLevelStorage.get(level);
		}
		catch (IndexOutOfBoundsException e)
		{
			// +1 确保正常取第0个元素
			//tagLevelStorage.ensureCapacity(level*2 + 1);	//似乎不起什么作用
			int size = tagLevelStorage.size();
			for (int i = 0; i < size; i++)
			{
				tagLevelStorage.add(new ArrayList<CommonTagNode>());
			}
			
			return tagLevelStorage.get(level);
		}
	}
	
	
	//========================================================================================
	//get/set方法组
	
	public static ArrayList<ArrayList<CommonTagNode>> getTagLevelStorage()
	{
		return tagLevelStorage;
	}
}
