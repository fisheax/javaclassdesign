package zust.java.Any.view.Listener;

import java.awt.event.MouseListener;

import zust.java.Any.view.Boundary;

public class CollectionListener implements MouseListener
{
	Boundary boundary;

	public CollectionListener(Boundary boundary)
	{
		this.boundary = boundary;
	}

	@Override
	public void mouseClicked(java.awt.event.MouseEvent e)
	{

	}

	@Override
	public void mousePressed(java.awt.event.MouseEvent e)
	{
		//System.out.println("收藏");
		boundary.addToFavor();
	}

	@Override
	public void mouseReleased(java.awt.event.MouseEvent e)
	{

	}

	@Override
	public void mouseEntered(java.awt.event.MouseEvent e)
	{

	}

	@Override
	public void mouseExited(java.awt.event.MouseEvent e)
	{

	}

}