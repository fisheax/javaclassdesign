package zust.java.lx.net.Blog;

import zust.java.lx.net.Parser.HtmlParser;

/**
 * 为了提取博客文章而设计的blog基类
 * @author muxue
 *
 */

public abstract class Blog
{
	//blog地址或者本地文件
	private String url = "";
	//HtmlParser对象
	protected HtmlParser parser = null;
	
	
	//Blog的构造器
	public Blog()
	{
		
	}
	
	
	public Blog(String url)
	{
		this.url = url;
		parser = new HtmlParser(url);
		parser.startParse();
	}
	
	//获取博客文章标题和内容
	public abstract String getTitle();
	public abstract String getText();
	
	
	//===================================================================================
	//get/set方法组
	
	public void setURL(String url)
	{
		this.url = url;
		parser = new HtmlParser(url);
		parser.startParse();
	}
	
	public String getUrl()
	{
		return url;
	}


	public HtmlParser getParser()
	{
		return parser;
	}
}
