package zust.java.Any.view.Listener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import zust.java.Any.view.Boundary;

public class RightClickOnTable extends MouseAdapter
{
	Boundary bou;

	@Override
	public void mouseClicked(MouseEvent e)
	{
		bou.rightClickActionPerformed(e);
	}

	public RightClickOnTable(Boundary b)
	{
		this.bou = b;
	}

}
