package zust.java.lx.net.Validator;

/**
 * html的标签名称验证器
 * @author muxue
 */

import java.util.ArrayList;

public class TagNameValidator
{
	public static ArrayList<String> tagNameArray = new ArrayList<String>();
	
	static
	{
		tagNameArray.add("html");
		tagNameArray.add("head");
		tagNameArray.add("title");
		tagNameArray.add("body");
		tagNameArray.add("div");
		tagNameArray.add("blockquote");
		tagNameArray.add("em");
		tagNameArray.add("strong");
		tagNameArray.add("cite");
		tagNameArray.add("code");
		tagNameArray.add("samp");
		tagNameArray.add("kbd");
		tagNameArray.add("var");
		tagNameArray.add("address");
		tagNameArray.add("big");
		tagNameArray.add("small");
		tagNameArray.add("b");
		tagNameArray.add("i");
		tagNameArray.add("u");
		tagNameArray.add("s");
		tagNameArray.add("sub");
		tagNameArray.add("sup");
		tagNameArray.add("tt");
		tagNameArray.add("pre");
		tagNameArray.add("center");
		tagNameArray.add("blink");
		tagNameArray.add("font");
		tagNameArray.add("basefont");
		tagNameArray.add("a");
		tagNameArray.add("img");
		tagNameArray.add("map");
		tagNameArray.add("area");
		tagNameArray.add("meta");
		tagNameArray.add("embed");
		tagNameArray.add("p");
		tagNameArray.add("br");
		tagNameArray.add("hr");
		tagNameArray.add("nohr");
		tagNameArray.add("nobr");
		tagNameArray.add("wbr");
		tagNameArray.add("ul");
		tagNameArray.add("li");
		tagNameArray.add("ol");
		tagNameArray.add("dl");
		tagNameArray.add("dt");
		tagNameArray.add("dd");
		tagNameArray.add("menu");
		tagNameArray.add("dir");
		tagNameArray.add("form");
		tagNameArray.add("input");
		tagNameArray.add("select");
		tagNameArray.add("option");
		tagNameArray.add("textarea");
		tagNameArray.add("table");
		tagNameArray.add("tr");
		tagNameArray.add("td");
		tagNameArray.add("th");
		tagNameArray.add("caption");
		tagNameArray.add("h1");
		tagNameArray.add("h2");
		tagNameArray.add("h3");
		tagNameArray.add("h4");
		tagNameArray.add("h5");
		tagNameArray.add("h6");
		tagNameArray.add("applet");
		tagNameArray.add("area");
		tagNameArray.add("button");
		tagNameArray.add("col");
		tagNameArray.add("colgroup");
		tagNameArray.add("del");
		tagNameArray.add("fieldset");
		tagNameArray.add("frame");
		tagNameArray.add("iframe");
		tagNameArray.add("ins");
		tagNameArray.add("label");
		tagNameArray.add("link");
		tagNameArray.add("noframes");
		tagNameArray.add("noscript");
		tagNameArray.add("object");
		tagNameArray.add("param");
		tagNameArray.add("q");
		tagNameArray.add("script");
		tagNameArray.add("span");
		tagNameArray.add("strike");
		tagNameArray.add("style");
		tagNameArray.add("sub");
		tagNameArray.add("tbody");
		tagNameArray.add("tfoot");
		tagNameArray.add("var");
		//and more
	}
	
	public static boolean validateTagName(String tagName)
	{
		for (String string : tagNameArray)
		{
			if (tagName.equalsIgnoreCase(string))
			{
				return true;
			}
		}
		return false;
		
		//暂时直接认为是合法的标签了
		//return true;
	}
}
