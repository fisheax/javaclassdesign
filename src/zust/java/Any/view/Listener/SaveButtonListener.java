package zust.java.Any.view.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import zust.java.Any.view.Boundary;

public class SaveButtonListener implements ActionListener
{

	Boundary boundary;

	public SaveButtonListener(Boundary boundary)
	{
		this.boundary = boundary;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if ("update".equals(e.getActionCommand()))
		{
			boundary.saveMenuItemActionPerformed(e);
		}
		else if ("save".equals(e.getActionCommand()))
		{
			boundary.insertIntoDB();
			boundary.getKeepButton().setActionCommand("update");
		}

	}

}
