package zust.java.jiangT.db.table;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import zust.java.jiangT.db.MySqlConnection;

/**
 * 
 * @ClassName: Table 
 * @Description:数据库中的表的模型
 * @author jiangT 
 * @date 2015年6月10日 下午9:16:29 
 *
 */
public class Table
{
	//表名
	private String name;
	//主码
	private String primaryKey;

	//private String ForginerKey;
	//列名和该列数据的数据类型烦人映射表
	private Map<String, Class<?>> colunmMapping;
	//表中所有列的集合
	private List<String> colunmLabel;

	/**
	 * 
	 * @Title: getPrimaryKey 
	 * @Description:  返回主码
	 * @param @return
	 * @return String 
	 * @throws
	 */
	public String getPrimaryKey()
	{
		return primaryKey;
	}

	/**
	 * 
	 * @Title: getName 
	 * @Description: 获得这个表的主码
	 * @param @return
	 * @return String 
	 * @throws
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * 
	 * @Title: getColunmLabel 
	 * @Description:  返回所有列的保存集合
	 * @param @return
	 * @return List<String> 
	 * @throws
	 */
	public List<String> getColunmLabel()
	{
		return colunmLabel;
	}

	/**
	 * 
	 * @Title: setColunmLabel 
	 * @Description:  向列集合中添加列
	 * @param @param colName
	 * @return void 
	 * @throws
	 */
	public void setColunmLabel(String colName)
	{
		this.colunmLabel.add(colName);
	}

	/**
	 * @Description: 带表名的构造方法
	 * @param @param name 表名
	 *
	 */
	public Table(String name)
	{
		this.name = name.trim();
		colunmMapping = new HashMap<String, Class<?>>();
		colunmLabel = new ArrayList<String>();
	}

	/**
	 * 
	 * @Title: getColunmMapping 
	 * @Description:  返回列与数据类型的映射表
	 * @param @return
	 * @return Map<String,Class<?>> 
	 * @throws
	 */
	public Map<String, Class<?>> getColunmMapping()
	{
		return colunmMapping;
	}

	/**
	 * 设置这个表的主码
	 */
	private void setPrimaryKey(String columnName)
	{
		this.primaryKey = columnName;
	}

	/**
	 * 
	 * @Title: setColumnType 
	 * @Description:  向列与数据类型映射表中添加映射关系
	 * @param @param columnName
	 * @param @param type
	 * @return void 
	 * @throws
	 */
	private void setColumnType(String columnName, Class<?> type)
	{
		colunmMapping.put(columnName, type);
	}

	public String hasKey(String key)
	{
		String exist = null;
		for (String colunmName : colunmLabel)
		{
			if (key.equals(colunmName))
			{
				exist = colunmName;
				break;
			}
		}
		return exist;
	}

	/**
	 * 
	 * @Title: init 
	//	 * @Description:  完成表中属性的初始化工作
	 * @param @throws SQLException
	 * @return void 
	 * @throws
	 */
	public void init() throws SQLException
	{
		Connection conn = MySqlConnection.getConnection();
		/*
		 * this part copy and edit form JFinal-src
		 * package:com.jfinal.plugin.activerecord;
		 */
		Statement stm = conn.createStatement();
		System.out.println("TableName=" + name);
		ResultSet rs = stm.executeQuery("select * from " + name + " where 1=2;");
		ResultSetMetaData rsmd = rs.getMetaData();
		for (int i = 1; i <= rsmd.getColumnCount(); i++)
		{
			String colName = rsmd.getColumnName(i);
			String colClassName = rsmd.getColumnClassName(i);
			if ("java.lang.String".equals(colClassName))
			{
				// varchar, char, enum, set, text, tinytext, mediumtext, longtext
				this.setColumnType(colName, java.lang.String.class);
			}
			else if ("java.lang.Integer".equals(colClassName))
			{
				// int, integer, tinyint, smallint, mediumint
				this.setColumnType(colName, java.lang.Integer.class);
			}
			else if ("java.lang.Long".equals(colClassName))
			{
				// bigint
				this.setColumnType(colName, java.lang.Long.class);
			}
			else if ("java.sql.Date".equals(colClassName))
			{
				// date, year
				this.setColumnType(colName, java.sql.Date.class);
			}
			else if ("java.lang.Double".equals(colClassName))
			{
				// real, double
				this.setColumnType(colName, java.lang.Double.class);
			}
			else if ("java.lang.Float".equals(colClassName))
			{
				// float
				this.setColumnType(colName, java.lang.Float.class);
			}
			else if ("java.lang.Boolean".equals(colClassName))
			{
				// bit
				this.setColumnType(colName, java.lang.Boolean.class);
			}
			else if ("java.sql.Time".equals(colClassName))
			{
				// time
				this.setColumnType(colName, java.sql.Time.class);
			}
			else if ("java.sql.Timestamp".equals(colClassName))
			{
				// timestamp, datetime
				this.setColumnType(colName, java.sql.Timestamp.class);
			}
			else if ("java.math.BigDecimal".equals(colClassName))
			{
				// decimal, numeric
				this.setColumnType(colName, java.math.BigDecimal.class);
			}
			else if ("[B".equals(colClassName))
			{
				// binary, varbinary, tinyblob, blob, mediumblob, longblob
				// project: print_info.content varbinary(61800);
				this.setColumnType(colName, byte[].class);
			}
			else
			{
				int type = rsmd.getColumnType(i);
				if (type == Types.BLOB)
				{
					this.setColumnType(colName, byte[].class);
				}
				else if (type == Types.CLOB || type == Types.NCLOB)
				{
					this.setColumnType(colName, String.class);
				}
				else
				{
					this.setColumnType(colName, String.class);
				}

			}
			this.setColunmLabel(colName);
		}
		rs = conn.getMetaData().getPrimaryKeys(null, null, name);
		if (rs.next())
		{
			setPrimaryKey(rs.getString("COLUMN_NAME"));
		}
		rs.close();
		stm.close();
	}
}
