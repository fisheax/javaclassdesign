package zust.java.Any.view.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import zust.java.Any.view.Boundary;

public class AddFavor implements ActionListener
{

	Boundary bounday;

	public AddFavor(Boundary boundary)
	{
		this.bounday = boundary;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		bounday.addToFavor();
	}

}
