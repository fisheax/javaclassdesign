package zust.java.lx.net.Log;

/**
 * HtmlLogger的日志格式化
 * @author muxue
 */

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class HtmlLoggerFormatter extends Formatter
{

	@Override
	public String format(LogRecord record)
	{
		return record.getLevel() + " --: " + record.getMessage();
	}

}
