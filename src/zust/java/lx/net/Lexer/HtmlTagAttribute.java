package zust.java.lx.net.Lexer;

/**
 * 标签属性名和值的键值对，用map的话感觉有点夸张了，就自己做个简单的
 * @author muxue
 *
 */

public class HtmlTagAttribute
{
	private String attrName = "";
	private String attrValue = "";

	
	//HtmlTagAttribute的构造器
	public HtmlTagAttribute(String attrName, String attrValue)
	{
		this.attrName = attrName;
		this.attrValue = attrValue;
	}
	
	
	
	//===========================================================================
	//get/set方法组
	
	public String getAttrName()
	{
		return attrName;
	}
	
	
	public String getAttrValue()
	{
		return attrValue;
	}
	
}
