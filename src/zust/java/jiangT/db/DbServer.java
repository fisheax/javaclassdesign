package zust.java.jiangT.db;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;

import zust.java.jiangT.db.bean.Bean;
import zust.java.jiangT.db.mapping.Mapping;
import zust.java.jiangT.db.table.Table;

/**
 * 
* @ClassName: DbServiser 
* @Description: 该类提供对数据库的增删改查功能
* @author jiangT 
* @date 2015年6月10日 下午8:02:23 
*
*/
public class DbServer
{

	private Connection conn = MySqlConnection.getConnection();
	private PreparedStatement pre;
	private ResultSet res;
	private Mapping mapping;

	public DbServer()
	{
		mapping = Mapping.getMapping();
	}

	private SqlBuilder sqlBuilder = new SqlBuilder();

	/**
	 * 
	 * @Title: getTableRowCount 
	 * @Description: 查询表中有多少行记录
	 * @param @param tableName 数据库中的表名
	 * @param @return 表中记录的行数，如果查询失败返回-1
	 * @return boolean 
	 * @throws
	 */
	public int getTableRowCount(String tableName)
	{
		int rows = -1;
		String sql = "select count(*) from " + tableName + ";";
		try
		{
			pre = conn.prepareStatement(sql);
			res = pre.executeQuery();
			if (res.next())
			{
				rows = res.getInt(1);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return rows;
	}

	/**
	 * 
	 * @Title: insert 
	 * @Description: 将一个对象信息插入数据库中 
	 * @param @param object
	 * @param @return 成功？true：false
	 * @return boolean 
	 * @throws
	 */
	public boolean insert(Bean object)
	{
		boolean succeed = false;
		Table table = mapping.getMappedTable(object.getClass());
		String sql = sqlBuilder.SqlForInsert(table, object);
		ArrayList<Object> paras = SqlBuilder.getPapras();
		try
		{
			pre = conn.prepareStatement(sql);
			for (int i = 0; i < paras.size(); i++)
			{
				/**
				 * 判断？所代表的数据类型
				 */
				Object para = paras.get(i);
				if (para instanceof String)
				{
					pre.setString(i + 1, (String) para);
				}
				else if (para instanceof java.util.Date)
				{
					pre.setDate(i + 1, new java.sql.Date(((java.util.Date) para).getTime()));
				}
				else if (para instanceof Integer)
				{
					pre.setInt(i + 1, (int) para);
				}
				else if (para instanceof Double)
				{
					pre.setDouble(1 + i, (double) para);
				}
				else if (para instanceof Float)
				{
					pre.setFloat(i + 1, (float) para);
				}
				else if (para instanceof Long)
				{
					pre.setLong(i, (long) para);
				}
				else if (para instanceof Boolean)
				{
					pre.setBoolean(i, (boolean) para);
				}
				else if (para instanceof Time)
				{
					pre.setTime(i + 1, (Time) para);
				}
				else if (para instanceof Timestamp)
				{
					pre.setTimestamp(i + 1, (Timestamp) para);
				}
				else if (para instanceof java.math.BigDecimal)
				{
					pre.setBigDecimal(i, (BigDecimal) para);
				}
				else if (para instanceof Byte[])
				{
					pre.setBytes(i + 1, (byte[]) para);
				}
				else if (para instanceof Byte)
				{
					pre.setByte(i + 1, (byte) para);
				}
			}
			int n = pre.executeUpdate();
			if (n >= 1)
			{
				succeed = true;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return succeed;
	}

	/**
	 * 
	 * @Title: update 
	 * @Description: 更新数据库中的一个对象的信息 
	 * @param @param obj 待更新对象
	 * @param @return 成功返回true 否则false
	 * @return boolean 
	 * @throws
	 */
	public boolean update(Bean obj)
	{
		boolean succeed = false;
		Table table = mapping.getMappedTable(obj.getClass());
		String sql = sqlBuilder.SqlForUptate(table, obj);
		ArrayList<Object> paras = SqlBuilder.getPapras();
		try
		{
			pre = conn.prepareStatement(sql);
			/**
			 * 判断？多代表的数据类型
			 */
			for (int i = 0; i < paras.size(); i++)
			{
				Object para = paras.get(i);
				if (para instanceof String)
				{
					pre.setString(i + 1, (String) para);
				}
				else if (para instanceof java.util.Date)
				{
					pre.setDate(i + 1, new java.sql.Date(((java.util.Date) para).getTime()));
				}
				else if (para instanceof Integer)
				{
					pre.setInt(i + 1, (int) para);
				}
				else if (para instanceof Double)
				{
					pre.setDouble(1 + i, (double) para);
				}
				else if (para instanceof Float)
				{
					pre.setFloat(i + 1, (float) para);
				}
				else if (para instanceof Long)
				{
					pre.setLong(i, (long) para);
				}
				else if (para instanceof Boolean)
				{
					pre.setBoolean(i, (boolean) para);
				}
				else if (para instanceof Time)
				{
					pre.setTime(i + 1, (Time) para);
				}
				else if (para instanceof Timestamp)
				{
					pre.setTimestamp(i + 1, (Timestamp) para);
				}
				else if (para instanceof java.math.BigDecimal)
				{
					pre.setBigDecimal(i, (BigDecimal) para);
				}
				else if (para instanceof Byte[])
				{
					pre.setBytes(i + 1, (byte[]) para);
				}
				else if (para instanceof Byte)
				{
					pre.setByte(i + 1, (byte) para);
				}
			}
			int n = pre.executeUpdate();
			if (n >= 1)
			{
				succeed = true;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return succeed;
	}

	/**
	 * 
	 * @Title: delete 
	 * @Description:  删除数据库中的一个对象信息
	 * @param @param obj
	 * @param @return 删除成功返回true，否则false
	 * @return boolean 
	 * @throws
	 */
	public boolean delete(Bean obj)
	{
		boolean succeed = false;
		Table table = mapping.getMappedTable(obj.getClass());
		String sql = sqlBuilder.SqlForDelete(table, obj);
		try
		{
			pre = conn.prepareStatement(sql);
			int n = pre.executeUpdate();
			if (n >= 1)
			{
				succeed = true;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return succeed;
	}

	/**
	 * 
	 * @Title: findBean 
	 * @Description:  查找数据库中某个对象的信息
	 * @param @param obj
	 * @param @return 返回找到的对象
	 * @return Bean 
	 * @throws
	 */
	public Bean findBean(Bean obj)
	{
		Table table = mapping.getMappedTable(obj.getClass());
		String sql = sqlBuilder.SqlForSelect(table, obj);
		try
		{

			pre = conn.prepareStatement(sql);
			res = pre.executeQuery();
			while (res.next())
			{
				for (int j = 0; j < table.getColunmLabel().size(); j++)
				{
					obj.setAttribute(table.getColunmLabel().get(j), res.getObject(j + 1));
				}
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		return obj;
	}

	/**
	 * 
	 * @Title: select 
	 * @Description:  传入sql命令 返回一个保存了对象集合的ArrayList
	 * @param @param sql
	 * @param @return 将满足查询条件的对象封装到ArrayList集合中 返回一个集合
	 * @return ArrayList<? extends Bean> 
	 * @throws
	 */
	@SuppressWarnings("rawtypes")
	public ArrayList<? extends Bean> select(String sql)
	{
		ArrayList<Bean> page = new ArrayList<Bean>();
		Bean bean;
		//Map<String, Object> attribute;
		try
		{
			pre = conn.prepareStatement(sql);
			res = pre.executeQuery();
			ResultSetMetaData rsmd = res.getMetaData();
			String tableName = rsmd.getTableName(1);
			int selectedColumnCount = rsmd.getColumnCount();
			ArrayList<String> columns = new ArrayList<String>();
			for (int i = 1; i <= selectedColumnCount; i++)
			{
				columns.add(rsmd.getColumnLabel(i));
			}

			Class beanClass = mapping.getBeanClass(tableName);
			while (res.next())
			{
				bean = (Bean) beanClass.newInstance();
				//attribute=new HashMap<String, Object>();
				for (int i = 0; i < selectedColumnCount; i++)
				{
					//bean.put(table.getColunmLabel().get(i), res.getObject(i+1));
					bean.setAttribute(columns.get(i), res.getObject(i + 1));
				}
				page.add(bean);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
		return page;
	}

	/**
	 * 返回表中最大的主码，如果失败返回-1
	 * @param tableName
	 * @return
	 */
	public int getMaxPrimaryKey(String tableName)
	{
		int max = -1;
		Table table = mapping.getMappedTable(mapping.getBeanClass(tableName));
		String sql = "select max " + table.getPrimaryKey() + " from " + tableName;
		try
		{
			pre = conn.prepareStatement(sql);
			res = pre.executeQuery();
			if (res.next())
			{
				max = res.getInt(1);
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return max;
	}
}
