package zust.java.lx.net.Blog;

/**
 * blog对象生产工厂，也是我的任务对外的接口。暂时只支持csdn的博客
 * @author muxue
 *
 */

public class BlogFactory
{
	//从本地文件解析
	public static Blog getLocalBlog(String filepath, BlogType type)
	{
		switch (type)
		{
		case CSDN:
			return new CsdnBlog(filepath);
		}
		
		return null;
	}
	
	
	//从URL中解析
	public static Blog getNetBlog(String url)
	{
		String tempStr = url.substring(0, 30);
		
		if (tempStr.contains("blog.csdn.net"))
		{
			//csdn
			return new CsdnBlog(url);
		}
		else
		{
			return null;
		}
	}
}
