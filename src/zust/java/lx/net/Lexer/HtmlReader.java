package zust.java.lx.net.Lexer;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Logger;

import zust.java.lx.net.Log.HtmlLogger;

/**
 * HtmlReader封装一些方法，便于词法过程中的读取
 * @author muxue
 *
 */

public class HtmlReader
{
	//保存输入流
	private BufferedReader bufferedReader = null;
	//日志记录
	Logger logger = HtmlLogger.getLogger();
	//网页流转换成字符串
	private String tempString = "";
	//当前字符串位置指示
	private int index = 0;
	
	//HtmlReader的构造器
	public HtmlReader()
	{
		//
	}
	
	public HtmlReader(BufferedReader reader)
	{
		bufferedReader = reader;
		//将HtmlFlow转换成String
		tempString = convertToString();
	}
	
	//过滤网页中重复的空格等，删除   --->>>  暂时没有实现过滤（也不明朗需不需要过滤）
	private String convertToString()
	{
		StringBuffer sb = new StringBuffer();  
        char[] tempChars = new char[512];
		
        try
		{
			while (bufferedReader.read(tempChars, 0, 512) != -1)  
			{  
			    sb.append(tempChars);  
			    //清空tempChars
			    tempChars = new char[512];
			}
		}
		catch (IOException e)
		{
			logger.severe("I/O Exception while converting HtmlFlow to String ...");
			return null;
		}  
        return sb.toString(); 
	}
	
	//================================================================================================
	
	/*
	 * 用于读取的各种方法
	 */
	
	//读取一个tag name
	public String readTagName()
	{
		StringBuffer buffer = new StringBuffer();
		char tempCh = readCharacter();
		
		while (!Character.isWhitespace(tempCh) && tempCh != '>' && tempCh != '<')
		{
			buffer.append(tempCh);
			tempCh = readCharacter();
		}
		
		if (tempCh == '>' || tempCh == '<')
		{
			//回退读掉的'>'或'<'
			index--;
		}
		
		return buffer.toString();
	}
	
	
	//读取一个字符，并递增index
	public char readCharacter()
	{
		return tempString.charAt(index++);
	}
	
	//读取当前字符前一个字符
	public char peekBackCharacter()
	{
		return tempString.charAt(index-1);
	}
	
	//读取一个字符而不移动index
	public char peekCharacter()
	{
		return tempString.charAt(index);
	}
	
	//读取指定偏移的字符
	public char peekCharacter(int offset)
	{
		return tempString.charAt(index+offset);
	}
	
	//读取当前字符后一个字符
	public char peekFrontCharacter()
	{
		return tempString.charAt(index+1);
	}

	//跳过空格，遇到非空格则停下
	public void skipSpaces()
	{
		char tempCh = peekCharacter();
		while (Character.isWhitespace(tempCh))
		{
			index++;
			tempCh = peekCharacter();
		}
		
		return;
	}
	
	//跳过n个字符
	public void skipCharacters(int n)
	{
		index += n;
		return;
	}
	
	//读取comment
	public String readComment()
	{
		int size = 16;
		int endOffset = 0;
		
		//跳过'--'
		index += 2;
		
		String temp = tempString.substring(index, Math.min(index+size, tempString.length()));
		
		//寻找注释结尾标志
		while ((endOffset = temp.indexOf("-->")) == -1)
		{
			size *= 2;
			temp = tempString.substring(index, Math.min(index+size, tempString.length()));
		}
		
		temp = tempString.substring(index, index+endOffset);
		//endOffset and "-->"
		index += endOffset + 3;
		
		return temp.trim();
	}
	
	//读取doctype
	public String readDoctype()
	{
		StringBuffer buffer = new StringBuffer();
		
		//跳过"doctype"
		index += 7;
		
		char tempCh = readCharacter();
		
		skipSpaces();
		
		while (tempCh != '>')
		{
			buffer.append(tempCh);
			tempCh = readCharacter();
		}
		
		return buffer.toString().trim();
	}
	
	//读取CDATA
	public String readCDATA()
	{
		int size = 16;
		int endOffset = 0;
		
		//跳过'CDATA[['
		index += 7;
		
		String temp = tempString.substring(index, Math.min(index+size, tempString.length()));
		
		//寻找注释结尾标志
		while ((endOffset = temp.indexOf("]]>")) == -1)
		{
			size *= 2;
			temp = tempString.substring(index, Math.min(index+size, tempString.length()));
		}
		
		temp = tempString.substring(index, index+endOffset);
		//endOffset and "]]>"
		index += endOffset + 3;
		
		return temp.trim();
	}
	
	
	//读取一个属性名
	public String readAttrName()
	{
		StringBuffer buffer = new StringBuffer();
		char tempCh = readCharacter();
		
		while (tempCh != '=')
		{
			buffer.append(tempCh);
			tempCh = readCharacter();
		}
		
		return buffer.toString().trim();
	}
	
	
	//读取一个属性值
	public String readAttrValue()
	{
		//不支持有空格组成的而不用引号括着的值
		
		StringBuffer attrValue = new StringBuffer();
		
		skipSpaces();
		if (peekCharacter() == '\"')
		{
			//跳过"
			index++;
			//由""括着的值
			char tempCh = readCharacter();
			
			while (tempCh != '\"')
			{
				attrValue.append(tempCh);
				tempCh = readCharacter();
			}
			
		}
		else if (peekCharacter() == '\'')
		{
			//跳过'
			index++;
			//由''括着的值
			char tempCh = readCharacter();
			
			while (tempCh != '\'')
			{
				attrValue.append(tempCh);
				tempCh = readCharacter();
			}
			
		}
		else 
		{
			//单个不包含空格的值
			char tempCh = readCharacter();
			
			while (!Character.isWhitespace(tempCh) && tempCh != '>')
			{
				attrValue.append(tempCh);
				tempCh = readCharacter();
			}
			
			if (tempCh == '>')
			{
				//回退读掉的'>'
				index--;
			}
		}
		
		return attrValue.toString().trim();
	}
	
	
	
	//=========================================================================================
	//get/set方法组
	
	//获取转换后的string
	public String getTempString()
	{
		return tempString;
	}
	
	//获取当前计数index
	public int getIndex()
	{
		return index;
	}
	
	//设置输入流
	public void setHtmlFlow(BufferedReader reader)
	{
		bufferedReader = reader;
		//将HtmlFlow转换成String
		tempString = convertToString();
	}
}
