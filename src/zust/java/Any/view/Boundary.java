package zust.java.Any.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import zust.java.Any.view.Listener.AddFavor;
import zust.java.Any.view.Listener.CheckListerner;
import zust.java.Any.view.Listener.DeleteListener;
import zust.java.Any.view.Listener.EditorButtonListener;
import zust.java.Any.view.Listener.ExitCollectionListener;
import zust.java.Any.view.Listener.ExitListener;
import zust.java.Any.view.Listener.GrabListener;
import zust.java.Any.view.Listener.HelpListener;
import zust.java.Any.view.Listener.OpenListener;
import zust.java.Any.view.Listener.RightClickOnTable;
import zust.java.Any.view.Listener.SaveAsListener;
import zust.java.Any.view.Listener.SaveButtonListener;
import zust.java.Any.view.Listener.SaveListener;
import zust.java.Any.view.Listener.SearchListener;
import zust.java.Any.view.Listener.SelectListener;
import zust.java.Any.view.bean.Blog;
import zust.java.Any.view.bean.Favorites;
import zust.java.Any.view.bean.MenuTable;
import zust.java.jiangT.db.DbServer;
import zust.java.jiangT.db.MySqlConnection;
import zust.java.jiangT.db.config.DbConfig;
import zust.java.lx.net.Blog.BlogFactory;
import zust.java.lx.net.Blog.BlogType;

public class Boundary extends JFrame
{

	private static final long serialVersionUID = 1L;
	private JFileChooser filechooser = new JFileChooser();
	private File file = null;
	DbServer dbServer = new DbServer();
	private static JTextArea contentArea = new JTextArea();
	private static JTextField label = new JTextField();
	private static int blogNow = 0;
	private MenuPanel menuPane;
	JTextField textField;
	JButton keepButton;

	public JButton getKeepButton()
	{
		return keepButton;
	}

	public JTextField getTextField()
	{
		return textField;
	}

	public MenuPanel getMenuPane()
	{
		return menuPane;
	}

	public static int getBlogNow()
	{
		return blogNow;
	}

	public static void setBlogNow(int blog)
	{
		blogNow = blog;
	}

	public static JTextArea getContentArea()
	{
		return contentArea;
	}

	public static JTextField getLabel()
	{
		return label;
	}

	public Boundary()
	{

		//申明目录，内容面板
		menuPane = new MenuPanel();
		menuPane.geTable().setModel(getData());

		//菜单栏设置
		JMenuBar mb = new JMenuBar();
		JMenu m1 = new JMenu("文件");
		JMenuItem open = new JMenuItem("打开");
		JMenuItem close1 = new JMenuItem("保存");
		JMenuItem close2 = new JMenuItem("另存为");
		JMenuItem exit = new JMenuItem("退出");
		m1.add(open);
		m1.add(close1);
		m1.add(close2);
		m1.add(exit);

		JMenu m2 = new JMenu("收藏夹");
		JMenuItem check = new JMenuItem("查看收藏");
		JMenuItem exitcollection = new JMenuItem("退出收藏");
		exitcollection.setEnabled(false);
		m2.add(check);
		m2.add(exitcollection);

		JMenu m3 = new JMenu("工具");
		JMenuItem grab = new JMenuItem("抓取");
		JMenuItem help = new JMenuItem("帮助");
		m3.add(grab);
		m3.add(help);

		mb.add(m1);
		mb.add(m2);
		mb.add(m3);

		//内容panel
		Panel contentPane = new Panel();
		contentPane.setLayout(new BorderLayout());
		JButton editorButton;

		Panel buttonPanel;
		Panel labelPanel;

		JScrollPane contentPanel = new JScrollPane(contentArea);
		buttonPanel = new Panel();

		editorButton = new JButton("编辑");
		keepButton = new JButton("保存");
		contentArea.setEditable(false);
		contentArea.setLineWrap(true);

		contentPane.setPreferredSize(new Dimension(300, 398));
		textField = new JTextField();
		textField.setEditable(false);
		buttonPanel.setLayout(new GridLayout(1, 3, 5, 0));
		buttonPanel.add(textField);
		buttonPanel.add(editorButton);
		buttonPanel.add(keepButton);

		labelPanel = new Panel();
		labelPanel.setLayout(new GridLayout(1, 1));

		label.setPreferredSize(new Dimension(470, 35));
		label.setEditable(false);
		labelPanel.add(label);
		contentPane.add(labelPanel, BorderLayout.NORTH);
		contentPane.add(contentPanel, BorderLayout.CENTER);
		contentPane.add(buttonPanel, BorderLayout.SOUTH);

		//添加panel
		setJMenuBar(mb);
		add(menuPane, BorderLayout.WEST);
		add(contentPane, BorderLayout.CENTER);

		//添加监听
		OpenListener openListener = new OpenListener(this);
		open.addActionListener(openListener);
		SaveListener close1Listener = new SaveListener(this);
		close1.addActionListener(close1Listener);
		SaveAsListener close2Listener = new SaveAsListener(this);
		close2.addActionListener(close2Listener);
		ExitListener exitListener = new ExitListener(this);
		exit.addActionListener(exitListener);
		check.addActionListener(new CheckListerner(this));
		ExitCollectionListener exitCollectionListener = new ExitCollectionListener(this);
		exitcollection.addActionListener(exitCollectionListener);
		GrabListener grabListener = new GrabListener(this);
		grab.addActionListener(grabListener);
		SaveButtonListener saveButtonListener = new SaveButtonListener(this);
		keepButton.addActionListener(saveButtonListener);

		HelpListener helpListener = new HelpListener();
		help.addActionListener(helpListener);
		EditorButtonListener editorButtonListener = new EditorButtonListener(this);
		editorButton.addActionListener(editorButtonListener);
		//添加表格右击监听
		menuPane.geTable().addMouseListener(new RightClickOnTable(this));
		menuPane.getSearchTextField().addKeyListener(new SearchListener(this));
	}

	//让JTextarea能编辑
	public void setEditor(ActionEvent e)
	{
		contentArea.setEditable(true);
	}

	@SuppressWarnings("unchecked")
	public void checkFavor()
	{
		String sqlForFavor = "select distinct blog_no from favorites ";
		String sqlForBlog = "select blog_no,title from blog where blog_no in(" + sqlForFavor + ");";
		MenuTable favorMenuTable = new MenuTable();
		MenuTable.setBlogTable((ArrayList<Blog>) dbServer.select(sqlForBlog));
		MenuTable.setMeunMapping();
		menuPane.geTable().setModel(favorMenuTable);
	}

	/**
	 * 根据博客标题去数据库中搜索
	 * @param e
	 */
	@SuppressWarnings("unchecked")
	public void menesearch(char nextChar)
	{
		String forSearch = menuPane.getSearchTextField().getText().trim() + (Character.isISOControl(nextChar) ? "" : nextChar);
		//System.out.println(forSearch);
		String sql = "select title,blog_no from blog where title like '%" + forSearch + "%';";
		MenuTable seachRes = new MenuTable();
		MenuTable.setBlogTable((ArrayList<Blog>) dbServer.select(sql));
		MenuTable.setMeunMapping();
		menuPane.geTable().setModel(seachRes);
	}

	//右击
	public void rightClickActionPerformed(MouseEvent e)
	{
		if (e.getButton() == MouseEvent.BUTTON3)
		{

			JPopupMenu popupMenu1 = new JPopupMenu();
			JMenuItem deleteItem = new JMenuItem("删除");
			JMenuItem selectItem = new JMenuItem("查看");
			JMenuItem addToFavortiesItem = new JMenuItem("添加到收藏夹");
			//删除监听
			deleteItem.addActionListener(new DeleteListener(this));
			//查看监听
			selectItem.addActionListener(new SelectListener(this));
			addToFavortiesItem.addActionListener(new AddFavor(this));
			popupMenu1.add(deleteItem);
			popupMenu1.add(selectItem);
			popupMenu1.add(addToFavortiesItem);
			popupMenu1.show(this.menuPane, e.getX(), e.getY());
		}
	}

	//向blog表中添加数据
	@SuppressWarnings("unchecked")
	public void insertIntoDB()
	{
		Blog blog = new Blog();
		blog.setAttribute("title", label.getText());
		blog.setAttribute("body", contentArea.getText());
		blog.setAttribute("comefrom", textField.getText());
		blog.setAttribute("create_date", new Date());
		blog.setAttribute("edit_date", new Date());

		if (dbServer.insert(blog))
		{
			JOptionPane.showMessageDialog(this, "添加成功");
			MenuTable seachRes = new MenuTable();
			MenuTable.setBlogTable((ArrayList<Blog>) dbServer.select("select * from blog"));
			MenuTable.setMeunMapping();
			menuPane.geTable().setModel(seachRes);
			contentArea.setText("");
			textField.setText("");
			label.setText("");
		}
		else
		{
			JOptionPane.showMessageDialog(this, "添加失败");
		}
	}

	//右击添加到收藏夹
	public void addToFavor()
	{
		int i = this.getMenuPane().geTable().getSelectedRow();
		
		if (i == -1)
		{
			JOptionPane.showMessageDialog(null, "请选中一行");
			return;
		}
		
		String infoString = (String) this.getMenuPane().geTable().getValueAt(i, 0);
		Blog blog = MenuTable.getBlog(infoString);
		Favorites favorites = new Favorites();
		favorites.setAttribute("blog_no", blog.getAttribute("blog_no"));
		favorites.setAttribute("collection_date", new Date());
		if (dbServer.insert(favorites))
		{
			JOptionPane.showMessageDialog(this, "添加成功");
		}
		else
		{
			JOptionPane.showMessageDialog(this, "添加失败");
		}

	}

	/**
	 * 右击菜单中的删除功能
	 * @param e
	 */
	@SuppressWarnings("unchecked")
	public void deleteRow(ActionEvent e)
	{
		int i = this.getMenuPane().geTable().getSelectedRow();
		
		if (i == -1)
		{
			JOptionPane.showMessageDialog(null, "请选中一行");
			return;
		}
		
		String infoString = (String) this.getMenuPane().geTable().getValueAt(i, 0);
		MenuTable menuTable = new MenuTable();
		Blog blog = MenuTable.getBlog(infoString);
		if (dbServer.delete(blog))
		{
			JOptionPane.showMessageDialog(this, "删除成功");
			MenuTable.setBlogTable((ArrayList<Blog>) dbServer.select("select title,blog_no from blog;"));
			MenuTable.setMeunMapping();
			this.getMenuPane().geTable().setModel(menuTable);
		}
		else
		{
			JOptionPane.showMessageDialog(this, "删除失败");
		}

	}

	/**
	 * 右击查看
	 * @param e
	 */
	public void showView(ActionEvent e)
	{
		int selrow = this.getMenuPane().geTable().getSelectedRow();
		
		if (selrow == -1)
		{
			JOptionPane.showMessageDialog(null, "请选中一行");
			return;
		}
		
		String selectedKey = (String) this.getMenuPane().geTable().getValueAt(selrow, 0);
		Blog blog = MenuTable.getBlog(selectedKey);
		DbServer dbServiser = new DbServer();
		if (!blog.checkAttribute("body"))
		{
			blog = (Blog) dbServiser.findBean(blog);
		}
		Boundary.getContentArea().setText((String) blog.getAttribute("body"));
		Boundary.getLabel().setText((String) blog.getAttribute("title"));
		this.getTextField().setText((String) blog.getAttribute("comefrom"));
		Boundary.setBlogNow((int) blog.getAttribute("blog_no"));
		Boundary.getContentArea().setEditable(false);
		Boundary.getLabel().setEditable(false);
	}

	//打开文件选择窗口
	public void openMenuItemActionPerformed(ActionEvent evt)
	{
		try
		{
			file = null;
			int returnVal = filechooser.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION)
			{
				file = filechooser.getSelectedFile();

				//解析本地网页内容
				zust.java.lx.net.Blog.Blog blog = BlogFactory.getLocalBlog(file.getPath(), BlogType.CSDN);
				keepButton.setActionCommand("save");
				getLabel().setText(blog.getTitle());
				contentArea.setText(blog.getText());
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	//抓取在线博客的内容，打开提示框，提示输入网址
	public void grabMenuItemActionPerformed(ActionEvent e)
	{
		String inputValue = JOptionPane.showInputDialog("暂时只支持csdn的blog");
		//解析在线的blog
		zust.java.lx.net.Blog.Blog blog = BlogFactory.getNetBlog(inputValue);
		getLabel().setText("");
		contentArea.setText("");
		getLabel().setEditable(false);
		contentArea.setEditable(false);
		getLabel().setText(blog.getTitle());
		contentArea.setText(blog.getText());
		textField.setText(blog.getUrl());
		zust.java.Any.view.bean.Blog b1 = new zust.java.Any.view.bean.Blog();
		b1.setAttribute("title", blog.getTitle());
		b1.setAttribute("body", blog.getText());
		keepButton.setActionCommand("save");
	}

	//保存事件方法
	public void saveMenuItemActionPerformed(ActionEvent evt)
	{
		//判断保存事件，是否需要保存

		String title = label.getText().trim();
		String body = contentArea.getText();
		String key = getBlogNow() + "_" + title;
		Blog blog = new Blog();
		blog.setAttribute("blog_no", getBlogNow());
		blog.setAttribute("title", title);
		blog.setAttribute("body", body);
		blog.setAttribute("edit_date", new Date());

		if (dbServer.update(blog))
		{
			MenuTable.getMenuMapTable().remove(key);
			MenuTable.putmenuMapTable(getBlogNow() + "_" + title, blog);
			menuPane.geTable().setModel(new MenuTable());
			JOptionPane.showMessageDialog(this, "保存成功");
		}
		else
		{
			JOptionPane.showMessageDialog(this, "保存失敗");
		}
	}

	//另存为事件方法
	public void saveAsMenuItemActionPerformed(ActionEvent evt)
	{
		filechooser.setDialogTitle("另存为...");
		int returnVal = filechooser.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION)
		{
			file = filechooser.getSelectedFile();
			try
			{
				FileWriter fw = new FileWriter(file);
				fw.write(label.getText() + "\r\n");
				fw.write(contentArea.getText());
				setTitle(filechooser.getSelectedFile().getName() + " - \u8bb0\u4e8b\u672c");
				fw.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	//退出事件方法
	public void exitMenuItem_actionPerformed(ActionEvent e)
	{
		//有文本
		if (!("".equals(contentArea.getText())))
		{
			//显示保存警告
			Object[] options = { " 是(Y) ", " 否(N) ", " 取消" };
			int s = JOptionPane.showOptionDialog(null, "文件的文件已经改变。\n想保存文件吗？", "\u8bb0\u4e8b\u672c", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
			switch (s)
			{
			//如果选择是-->要保存
			case 0:
				int returnVal = filechooser.showSaveDialog(this);
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
					file = filechooser.getSelectedFile();
					try
					{
						FileWriter fw = new FileWriter(file);
						fw.write(contentArea.getText());
						setTitle(filechooser.getSelectedFile().getName() + " - \u8bb0\u4e8b\u672c");
						fw.close();
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
					}
				}
				MySqlConnection.closeConnection();
				System.exit(0);
			case 1:
				MySqlConnection.closeConnection();
				System.exit(0);
			}
			//else:如果没有文本的话直接退出
		}
		else
		{
			MySqlConnection.closeConnection();
			System.exit(0);
		}
	}

	//得到数据
	@SuppressWarnings("unchecked")
	private MenuTable getData()
	{
		DbServer dbServiser = new DbServer();
		MenuTable menuTable = new MenuTable();
		MenuTable.setBlogTable((ArrayList<Blog>) (dbServiser.select("select blog_no,title from blog;")));
		MenuTable.setMeunMapping();
		return menuTable;
	}

	public DbServer getDbServer()
	{
		return dbServer;
	}

	public static void main(String[] args)
	{
		DbConfig.init();
		Boundary test = new Boundary();


		test.setTitle("网页抓取");
		ImageIcon icon = new ImageIcon("Article.png");
		
		test.setIconImage(icon.getImage());
		test.setSize(600, 500);
		test.setLocationRelativeTo(null);
		test.setResizable(false);
		test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		test.setVisible(true);

	}

}