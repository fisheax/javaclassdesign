package zust.java.lx.net.Log;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * HtmlParser的日志记录
 * @author muxue
 *
 */

public class HtmlLogger
{
	//默认开启日志记录
	private static boolean enableLogger = true;
	//日志记录级别
	private static Level level = Level.INFO;
	//单例模式
	private static HtmlLogger logger = new HtmlLogger();

	//采用java.util.logging.Logger
	private Logger infoLogger = null;

	//HtmlLogger的构造器
	private HtmlLogger()
	{
		infoLogger = Logger.getLogger("HtmlLogger.info");
		infoLogger.setLevel(HtmlLogger.level);
	}

	//获取Logger
	public static Logger getLogger()
	{
		if (enableLogger)
		{
			return logger.infoLogger;
		}
		else
		{
			return null;
		}
	}
	
	//获取HtmlLogger
	public static HtmlLogger getHtmlLogger()
	{
		return logger;
	}

	//设置是否开启日志记录
	public static void setEnableLogger(boolean status)
	{
		enableLogger = status;
	}

	//设置是否开启文件日志记录
	public static void setEnableFileLogger(boolean status)
	{
		if (status)
		{
			FileHandler fileHandler;
			try
			{
				//TODO:这里的文件位置再挪一挪
				//文件日志
				fileHandler = new FileHandler("C:/HtmlLogger.log");
				fileHandler.setLevel(Level.INFO);
				fileHandler.setFormatter(new HtmlLoggerFormatter());
				HtmlLogger.getLogger().addHandler(fileHandler);
			}
			catch (SecurityException e)
			{
				System.out.println("Create FileHandler Error ...");
			}
			catch (IOException e)
			{
				System.out.println("Create FileHandler Error ...");
			}
		}
	}
	
	//设置日志记录级别
	public static void setLevel(Level level)
	{
		HtmlLogger.level = level;
	}
	
	//获取日志记录级别
	public static Level getLevel()
	{
		return level;
	}
}
