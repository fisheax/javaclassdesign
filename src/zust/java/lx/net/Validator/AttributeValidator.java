package zust.java.lx.net.Validator;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * html标签属性的验证器
 * @author muxue
 *
 */

public class AttributeValidator
{
	public static HashMap<String, ArrayList<String>> attributeMap = new HashMap<String, ArrayList<String>>();
	
	static
	{
		//html
		ArrayList<String> htmlArray = new ArrayList<String>();
		htmlArray.add("id");
		htmlArray.add("class");
		////maybe still has more attributes
		attributeMap.put("html", htmlArray);
		
		//head
		ArrayList<String> headArray = new ArrayList<String>();
		headArray.add("id");
		htmlArray.add("class");
		////maybe still has more attributes
		attributeMap.put("head", headArray);
		
		//and more ...
	}
	
	public static boolean validateTagAttr(String tagName, String attrName)
	{
		/*ArrayList<String> tempArray = attributeMap.get(tagName.toLowerCase());
		//如果通过了标签名称的验证，这边应该是可以取到的，不再进行判断
		for (String string : tempArray)
		{
			if (attrName.equalsIgnoreCase(string))
			{
				return true;
			}
		}*/
		
		//这里暂不进行验证，直接通过验证
		return true;
	}
}
