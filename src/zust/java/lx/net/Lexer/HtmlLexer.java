package zust.java.lx.net.Lexer;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Stack;
import java.util.logging.Logger;

import zust.java.lx.net.Log.HtmlLogger;
import zust.java.lx.net.TagElement.CommentTagElement;
import zust.java.lx.net.TagElement.CommonTagElement;
import zust.java.lx.net.TagElement.DoctypeTagElement;
import zust.java.lx.net.TagNode.CommonTagNode;
import zust.java.lx.net.TagNode.SpecialTagNode;
import zust.java.lx.net.TagNode.TagNodeFactory;
import zust.java.lx.net.Validator.AttributeValidator;
import zust.java.lx.net.Validator.TagNameValidator;

/**
 * html文档词法解析器
 * 
 * 说明
 * 1. html标签前后还是可以有其他标签的，这些标签也是会被解析的
 * 2. 行为改变，本词法解析器不再单纯做解析工作，同时也将组建DOM树
 * 
 * @author muxue
 *
 */

public class HtmlLexer
{
	//html文档流
	private BufferedReader htmlFlow = null;
	//HtmlReader
	private HtmlReader reader = null;
	//html文档流转成字符串的长度
	private int length = 0;
	//html logger
	private Logger logger = HtmlLogger.getLogger();
	
	
	//词法分析后的元素保存到栈里
	private Stack<Object> eleStack = new Stack<Object>();
	//注释集合
	private ArrayList<SpecialTagNode> commentArray = new ArrayList<SpecialTagNode>();
	//文档类型集合
	private ArrayList<SpecialTagNode> doctypeArray = new ArrayList<SpecialTagNode>();
	//CDATA集合
	private ArrayList<SpecialTagNode> cdataArray = new ArrayList<SpecialTagNode>();
	
	
	//Html词法分析状态
	private HtmlTagStatus status = HtmlTagStatus.ReadData;
	//可以选择不关闭的标签集合
	private ArrayList<String> notNeedClosedTags = new ArrayList<String>();
	//不对内容进行解析的标签集合
	private ArrayList<String> notParseContentTags = new ArrayList<String>();
	//标志：词法分析器是否正常退出
	public static boolean isOrdQuit = true;
	//当前标签所在层级数
	private int level = 0;
	//当前的标签名称
	private String curTagName = "";
	//当前的属性名称
	private String curAttrName = "";
	
	
	
	//HtmlLexer的构造器
	public HtmlLexer()
	{
	}
	
	public HtmlLexer(BufferedReader reader)
	{
		htmlFlow = reader;
		this.reader = new HtmlReader(htmlFlow);
		length =  this.reader.getTempString().length();
		addDefaultNotNeedClosedTags();
		addDefaultNotNotParseContentTags();
	}
	
	
	//进行词法分析
	public void startLexer()
	{
		//
		while (status != HtmlTagStatus.AllFinished)
		{
			//状态处理分发
			StatusDispatcher();
		}
		
		return;
	}

	//=========================================================================================
	
	/*
	 * 状态处理方法
	 */
	
	//状态处理分发
	private void StatusDispatcher()
	{
		switch (status)
		{
		case ReadData:
			ReadDataHandler();
			break;
			
		case TagOpen:
			TagOpenHandler();
			break;
			
		case TagEnd:
			TagEndHandler();
			break;
			
		case TagClosed:
			TagClosedHandler();
			break;
			
		case MarkTagOpen:
			MarkTagOpenHandler();
			break;
			
		case TagName:
			TagNameHandler();
			break;
			
		case TagSelfClose:
			TagSelfCloseHandler();
			break;
			
		case AttrName:
			AttrNameHandler();
			break;
			
		case AttrValue:
			AttrValueHandler();
			break;

		case TagInvalid:
			TagInvalidHandler();
			break;
		default:
			logger.warning("Should never be here ...");
			break;
		}
	}
	
	//ReadDataHandler
	private void ReadDataHandler()
	{
		StringBuffer tempBuffer = new StringBuffer();
		char ret = '\0';
		
		while(reader.getIndex() < length && (ret = reader.readCharacter()) != '<')
		{
			tempBuffer.append(ret);
		}
		
		if (ret == '<')
		{
			status = HtmlTagStatus.TagOpen;
		}
		
		if (reader.getIndex() == length)
		{
			status = HtmlTagStatus.AllFinished;
			logger.info("Lexing & building DOM tree have done, stopping lexer ...");
			return;
		}
		
		//如果两个标签之间</h1> <p>都是由空白符组成，则忽略掉
		boolean isValid = false;
		String str = tempBuffer.toString();
		
		for (int i=0; i<str.length(); i++)
		{
			if (!Character.isWhitespace(str.charAt(i)))
			{
				isValid = true;
				break;
			}
		}
		
		if (isValid)
		{
			eleStack.push(tempBuffer.toString());
		}
		
		return;
	}
	
	
	//TagOpenHandler
	private void TagOpenHandler()
	{
		reader.skipSpaces();
		
		char tempCh = reader.peekCharacter();
		
		if (Character.isLetter(tempCh))
		{
			status = HtmlTagStatus.TagName;
		}
		else if (tempCh == '!') 
		{
			status = HtmlTagStatus.MarkTagOpen;
		}
		else if(tempCh == '/') 
		{
			status = HtmlTagStatus.TagClosed;
		}
		else 
		{
			//status = HtmlTagStatus.TagInvalid;
			//普通文本中使用 '<' ， 后面跟的是非字母
			eleStack.push("<");
			status = HtmlTagStatus.ReadData;
		}
		
		return;
	}
	
	
	//TagEndHandler
	private void TagEndHandler()
	{
		//跳过'>'
		reader.skipCharacters(1);
		
		//额外处理无须关闭的标签
		if (notNeedClosedTags.contains(curTagName))
		{
			String tempString = reader.getTempString().substring(reader.getIndex(), reader.getIndex()+30).replaceAll("\\s", "");
			int tempIndex = tempString.indexOf("<");
			int i = 2;
			
			while (tempIndex == -1)
			{
				tempString = reader.getTempString().substring(reader.getIndex(), 
						(reader.getIndex()+30*i) > reader.getTempString().length() ? 
								reader.getTempString().length()-1 : 
									reader.getIndex()+30*i).replaceAll("\\s", "");
				tempIndex = tempString.indexOf("<");
				i++;
			}
			
			if (!tempString.substring(tempIndex).startsWith(curTagName))
			{
				//是另外的标签，可以结束当前标签，组建节点对象
				buildTagNode();
			}
		}
		
		//额外处理内容可以包含html标签的标签，script和pre，这两个要分开处理撒~~
		//失误：pre只是个普通标签
		
		//script
		if ("script".equalsIgnoreCase(curTagName))
		{
			boolean doubleQuoteFlag = true;
			boolean singleQuoteFlag = true;
			StringBuffer buffer = new StringBuffer();
			char tempCh = reader.readCharacter();
			
			while (true)
			{
				if (tempCh == '\"')
				{
					if (reader.peekCharacter(-2) == '\\')
					{
						//js中的转义
						buffer.append(tempCh);
					}
					else 
					{
						doubleQuoteFlag = !doubleQuoteFlag;
					}
				}
				else if (tempCh == '\'') 
				{
					if (reader.peekCharacter(-2) == '\\')
					{
						//js中的转义
						buffer.append(tempCh);
					}
					else 
					{
						singleQuoteFlag = !singleQuoteFlag;
					}
				}
				else if (tempCh == '<')
				{
					if (reader.peekCharacter() == '/' && doubleQuoteFlag && singleQuoteFlag)
					{
						//script标签结束
						//回退一个字符 --> '<'
						reader.skipCharacters(-1);
						break;
					}
					else
					{
						buffer.append(tempCh);
					}
				}
				else 
				{
					buffer.append(tempCh);
				}
				
				tempCh = reader.readCharacter();
			}//end while
			
			eleStack.push(buffer.toString());
		}
		
		
		status = HtmlTagStatus.ReadData;
	}
	
	
	//MarkTagOpenHandler
	private void MarkTagOpenHandler()
	{
		//跳过'!'
		reader.skipCharacters(1);
		
		String subVerStr = reader.getTempString().substring(reader.getIndex(), reader.getIndex()+7).toLowerCase();
		
		if (subVerStr.startsWith("--"))
		{
			//comment
			commentArray.add(TagNodeFactory.getSpecialTagNode(new CommentTagElement(reader.readComment())));
			status = HtmlTagStatus.ReadData;
		}
		else if (subVerStr.startsWith("doctype"))
		{
			//doctype
			doctypeArray.add(TagNodeFactory.getSpecialTagNode(new DoctypeTagElement(reader.readDoctype())));
			status = HtmlTagStatus.ReadData;
		}
		else if (subVerStr.startsWith("cdata"))
		{
			//CDATA
			cdataArray.add(TagNodeFactory.getSpecialTagNode(new DoctypeTagElement(reader.readCDATA())));
			status = HtmlTagStatus.ReadData;
		}
		else 
		{
			logger.severe("Unsupport ! instruction ...");
			status = HtmlTagStatus.AllFinished;
			isOrdQuit = false;
		}
		
		return;
	}
	
	
	//TagNameHandler
	private void TagNameHandler()
	{
		String tagName = reader.readTagName();
		
		if (!TagNameValidator.validateTagName(tagName))
		{
			//非法标签，直接结束解析
			//status = HtmlTagStatus.AllFinished;
			//isOrdQuit = false;
			//logger.severe("Invalid Tag" + tagName + " stopping lexer ...");
			//return;
			
			//不当做非法标签，作为普通字符串入栈
			eleStack.push("<" + tagName);
			status = HtmlTagStatus.ReadData;
			
			return;
		}
		
		curTagName = tagName;
		eleStack.push("<" + tagName + ">");
		level++;
		
		reader.skipSpaces();
		
		char tempCh = reader.peekCharacter();
		
		if (Character.isLetter(tempCh))
		{
			status = HtmlTagStatus.AttrName;
		}
		else if (tempCh == '/') 
		{
			status = HtmlTagStatus.TagSelfClose;
		}
		else if (tempCh == '>')
		{
			status = HtmlTagStatus.TagEnd;
		}
		else 
		{
			status = HtmlTagStatus.TagInvalid;
		}
		
		return;
	}
	
	
	//TagSelfCloseHandler
	private void TagSelfCloseHandler()
	{
		//跳过'/'
		reader.skipCharacters(1);
		//这里存在空格也是合法的
		reader.skipSpaces();
		//这里应该是'>'
		
		if (reader.readCharacter() == '>')
		{
			status = HtmlTagStatus.ReadData;

			//一个标签结束，就要开始组建节点
			// level-- 之后应该查看下面第二级是否有元素，如果有，使之成为自己的子节点，然后清空相应层
			//封装到buildTagNode()中
			
			buildTagNode();
		}
		else
		{
			status = HtmlTagStatus.AllFinished;
			isOrdQuit = false;
			logger.severe("Syntax Error : TagSelfClose should be like with '/>'");
		}
		
		return;
	}
	
	
	//TagClosedHandler
	private void TagClosedHandler()
	{
		//跳过'/'
		reader.skipCharacters(1);
		
		String tagName = reader.readTagName();
		if (!TagNameValidator.validateTagName(tagName))
		{
			//非法标签，直接结束解析
			status = HtmlTagStatus.AllFinished;
			isOrdQuit = false;
			logger.severe("Invalid Tag" + tagName + ", stopping lexer ...");
			return;
		}
		
		//这里必须设置，否则碰上连续的</></>...原来的curTagName就会被覆盖掉
		curTagName = tagName;
		
		//一个标签结束，就要开始组建节点
		// level-- 之后应该查看下面第二级是否有元素，如果有，使之成为自己的子节点，然后清空相应层
		//封装到buildTagNode()中
		
		buildTagNode();
		
		
		reader.skipSpaces();
		
		if (reader.readCharacter() == '>')
		{
			status = HtmlTagStatus.ReadData;
		}
		else 
		{
			//标签非法结尾，直接结束解析
			status = HtmlTagStatus.AllFinished;
			isOrdQuit = false;
			logger.severe("Invalid Tag End " + tagName + " stopping lexer ...");
			return;
		}
		
		return;
	}
	
	
	//AttrNameHandler
	private void AttrNameHandler()
	{
		String attrName = reader.readAttrName();
		if (!AttributeValidator.validateTagAttr(curTagName, attrName))
		{
			//非法属性，直接结束解析
			status = HtmlTagStatus.AllFinished;
			isOrdQuit = false;
			logger.severe("Invalid Tag Attribute " + curTagName + " : " + attrName + " stopping lexer ...");
			return;
		}

		//记录当前的属性名，用于后面和属性值组成一个键值对，然后进入栈里
		curAttrName = attrName;
		
		//不知道有没有只有属性而没有值得东西。。。这里就认为是没有这样子的属性
		status = HtmlTagStatus.AttrValue;
		
		return;
	}
	
	
	//AttrValueHandler
	private void AttrValueHandler()
	{
		String attrValue = reader.readAttrValue();
		
		//和当前属性组成键值对，放到ArrayList里
		eleStack.push(new HtmlTagAttribute(curAttrName, attrValue));
		curAttrName = "";
		
		
		//
		reader.skipSpaces();
		
		char tempCh = reader.peekCharacter();

		if (Character.isLetter(tempCh))
		{
			status = HtmlTagStatus.AttrName;
		}
		else if (tempCh == '/') 
		{
			status = HtmlTagStatus.TagSelfClose;
		}
		else if (tempCh == '>')
		{
			status = HtmlTagStatus.TagEnd;
		}
		else 
		{
			status = HtmlTagStatus.TagInvalid;
		}
		
		return;
	}
	
	//TagInvalidHandler
	private void TagInvalidHandler()
	{
		logger.severe("Invalid Html Tag ...");
		//对于非法标签就直接中断执行过程了
		//设置非法退出标志
		isOrdQuit = false;
		status = HtmlTagStatus.AllFinished;
	}
	
	
	//组建节点
	private void buildTagNode()
	{

		CommonTagElement element = new CommonTagElement(curTagName);
		CommonTagNode node = TagNodeFactory.getCommonTagNode(element);
		StringBuffer content = new StringBuffer();
		Object object = null;
		
		while (true)
		{
			if (eleStack.isEmpty())
			{
				break;
			}
			
			object = eleStack.pop();
			
			
			//属性键值对
			if (object instanceof HtmlTagAttribute)
			{
				element.getAttrs().put(((HtmlTagAttribute)object).getAttrName(), ((HtmlTagAttribute)object).getAttrValue());
			}
			//
			else if(object instanceof String)
			{
				//标签名称
				if (((String)object).toLowerCase().equals(("<" + curTagName + ">").toLowerCase()))
				{
					break;
				}
				//正文内容
				else
				{
					content.insert(0, object);
				}
			}
		}
		
		//设置id属性
		if (element.getAttrs().containsKey("id"))
		{
			node.setIdValue(element.getAttrs().get("id"));
		}
		
		//设置class属性
		if (element.getAttrs().containsKey("class"))
		{
			node.setClassValue(element.getAttrs().get("class"));
		}
		
		//设置正文
		element.setText(content.toString());
		level--;
		
		//不是最顶层
		if (level >= -1)
		{
			//检测下面一层是否有子元素
			ArrayList<CommonTagNode> tagLevel = HtmlTagLevelStorage.getTagLevel(level + 1);
			if (!tagLevel.isEmpty())
			{
				//有子元素

				CommonTagNode tempNode = null;

				for (int i = 0; i < tagLevel.size(); i++)
				{
					tempNode = tagLevel.get(i);

					//设置父节点
					tempNode.setParentNode(node);
					//设置子节点
					node.getChildNodes().add(tempNode);
					//设置右兄弟节点
					if (i < tagLevel.size() - 1)
					{
						tempNode.setRightBrotherNode(tagLevel.get(i + 1));
					}
					//设置左兄弟节点
					if (i != 0)
					{
						tempNode.setLeftBrotherNode(tagLevel.get(i - 1));
					}
				}
			}
			//
			curTagName = "";
			//下面一层清空
			tagLevel.clear();
			//最后数据都会留在第0层
			HtmlTagLevelStorage.getTagLevel(level).add(node);
		}
		//For debug
		//System.out.println(node);

		return;
	}
	
	
	//添加无须关闭的标签
	public void addNotNeedClosedTag(String tagName)
	{
		if (TagNameValidator.validateTagName(tagName))
		{
			notNeedClosedTags.add(tagName);
		}
	}
	
	//添加默认无须关闭的标签
	private void addDefaultNotNeedClosedTags()
	{
		notNeedClosedTags.add("link");
		notNeedClosedTags.add("br");
		notNeedClosedTags.add("hr");
		notNeedClosedTags.add("img");
		notNeedClosedTags.add("input");
		notNeedClosedTags.add("meta");
		notNeedClosedTags.add("center");
	}
	
	
	//添加不解析内容的标签
	public void addNotNotParseContentTag(String tagName)
	{
		if (TagNameValidator.validateTagName(tagName))
		{
			notParseContentTags.add(tagName);
		}
	}
	
	//添加默认不解析内容的标签
	private void addDefaultNotNotParseContentTags()
	{
		//
	}
	
	//=========================================================================================
	//get/set方法组
	
	//设置html文档流
	public void setHtmlReader(BufferedReader reader)
	{
		htmlFlow = reader;
		this.reader = new HtmlReader(htmlFlow);
		length =  this.reader.getTempString().length();
		addDefaultNotNeedClosedTags();
		addDefaultNotNotParseContentTags();
	}
	
	//获取词法分析元素
	public Stack<Object> getEleStack()
	{
		return eleStack;
	}
	
	//获取注释集合
	public ArrayList<SpecialTagNode> getCommentArray()
	{
		return commentArray;
	}
	
	//获取doctype集合
	public ArrayList<SpecialTagNode> getDoctypeArray()
	{
		return doctypeArray;
	}
	
	//获取CDATA集合
	public ArrayList<SpecialTagNode> getCDATAArray()
	{
		return cdataArray;
	}

	//获取无须闭合的标签集合
	public ArrayList<String> getNotNeedClosedTags()
	{
		return notNeedClosedTags;
	}

	
	//设置无须闭合的标签集合
	public void setNotNeedClosedTags(ArrayList<String> notNeedClosedTags)
	{
		this.notNeedClosedTags = notNeedClosedTags;
	}

	
	//获取不解析内容的标签集合
	public ArrayList<String> getNotParseContentTags()
	{
		return notParseContentTags;
	}

	//设置不解析内容的标签集合
	public void setNotParseContentTags(ArrayList<String> notParseContentTags)
	{
		this.notParseContentTags = notParseContentTags;
	}
}
