package zust.java.lx.net.TagNode;

import zust.java.lx.net.TagElement.CommonTagElement;
import zust.java.lx.net.TagElement.SpecialTagElement;

/**
 * DOM节点工厂
 * @author muxue
 *
 */

public class TagNodeFactory
{
	
	//创建一个普通标签节点对象
	public static CommonTagNode getCommonTagNode(CommonTagElement element)
	{
		return new CommonTagNode(element);
	}
	
	//创建一个特殊标签节点对象
	public static SpecialTagNode getSpecialTagNode(SpecialTagElement element)
	{
		return new SpecialTagNode(element);
	}
}
