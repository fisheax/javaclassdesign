package zust.java.lx.net.Filter;

import java.util.ArrayList;

import zust.java.lx.net.TagNode.CommonTagNode;
import zust.java.lx.net.Validator.TagNameValidator;

/**
 * 
 * 用于过滤的基类
 * @author muxue
 *
 */

public abstract class HtmlTagFilter
{
	//过滤规则（参考css选择器，目前实现简单的几个）
	/*
	 * .class 			.intro 			选择 class="intro" 的所有元素
	 * #id		 		#firstname 		选择 id="firstname"的所有元素
	 * * 								选择所有元素。 						//默认就是不加以过滤
	 * element 			p 				选择所有 <p> 元素
	 * element,element 	div,p 			选择所有<div> 元素和所有 <p> 元素。 	//群组和后代选择器应该不能混用吧
	 * element element 	div p 			选择 <div> 元素内部的所有 <p> 元素。
	 */
	
	//选择器规则
	private String selectorText = "";
	//解析后的规则
	private ArrayList<String> selectors = new ArrayList<String>();
	
	//HtmlTagFilter的构造器
	public HtmlTagFilter()
	{
		
	}
	
	public HtmlTagFilter(String selector)
	{
		selectorText = selector.trim();
		selectors = parseSelectorText();
	}
	
	//解析选择器规则
	private ArrayList<String> parseSelectorText()
	{
		ArrayList<String> selectors = new ArrayList<String>();
		String[] elements;
		
		
		if (selectorText.contains(","))
		{
			//群组选择器
			elements = selectorText.split(",");
		}
		else
		{
			//后代选择器
			elements = selectorText.split("\\s");
		}
		
		//
		for (String string : elements)
		{
			//防止规则中有连续的空白符
			if (string.replaceAll("\\s*", "").equals(""))
			{
				continue;
			}
			
			//验证 tag name
			if (!(string.startsWith(".") || string.startsWith("#") || TagNameValidator.validateTagName(string)))
			{
				continue;
			}
			
			selectors.add(string);
		}
		
		return selectors;
	}
	
	//过滤函数
	public abstract boolean filter(CommonTagNode currentNode);
	
	
	//=====================================================================================
	//get/set方法组
	
	//
	public String getSelectorText()
	{
		return selectorText;
	}

	//
	public void setSelectorText(String selectorText)
	{
		this.selectorText = selectorText;
	}

	
	//
	public ArrayList<String> getSelectors()
	{
		return selectors;
	}
}
