package zust.java.Any.view.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import zust.java.Any.view.Boundary;
import zust.java.Any.view.bean.Blog;
import zust.java.Any.view.bean.MenuTable;

public class ExitCollectionListener implements ActionListener
{
	Boundary boundary;

	public ExitCollectionListener(Boundary boundary)
	{
		this.boundary = boundary;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void actionPerformed(ActionEvent e)
	{
		MenuTable seachRes = new MenuTable();
		MenuTable.setBlogTable((ArrayList<Blog>) boundary.getDbServer().select("select * from blog"));
		MenuTable.setMeunMapping();
		boundary.getMenuPane().geTable().setModel(seachRes);
		
		boundary.getJMenuBar().getMenu(1).getItem(0).setEnabled(true);
		boundary.getJMenuBar().getMenu(1).getItem(1).setEnabled(false);
	}


}