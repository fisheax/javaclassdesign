package zust.java.lx.net.Parser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;

import zust.java.lx.net.DomTree.HtmlDomTree;
import zust.java.lx.net.Filter.HtmlTagFilter;
import zust.java.lx.net.Lexer.HtmlLexer;
import zust.java.lx.net.Lexer.HtmlTagLevelStorage;
import zust.java.lx.net.Log.HtmlLogger;
import zust.java.lx.net.TagElement.CommonTagElement;
import zust.java.lx.net.TagNode.CommonTagNode;
import zust.java.lx.net.TagNode.SpecialTagNode;

/**
 * html parser主类
 * @author muxue
 *
 */

public class HtmlParser
{
	/*
	 * 设置日志相关
	 * file from internet or local
	 * 创建词法解析器
	 * 构建DOM树
	 * 提供操作DOM树的方法
	 */
	

	public static final String UTF8 = "UTF-8";
	
	//文件URL
	private String url = null;
	//日志对象
	private Logger logger = null;
	//词法解析器
	private HtmlLexer lexer = null;
	//DOM树对象
	private HtmlDomTree domTree = null;
	//
	
	
	//HtmlParser构造器
	public HtmlParser()
	{
		logger = HtmlLogger.getLogger();
		domTree = new HtmlDomTree();
	}
	
	public HtmlParser(String url)
	{
		this.url = url;
		logger = HtmlLogger.getLogger();
		domTree = new HtmlDomTree();
	}
	
	//启动解析进程
	public void startParse()
	{
		if ("".equals(url))
		{
			logger.severe("URL can not be null");
		}
		else
		{
			//获取输入流
			BufferedReader htmlReader = openConnection();
			//创建词法解析器
			lexer = new HtmlLexer(htmlReader);
			if (lexer == null)
			{
				logger.severe("Create Lexer Error ...");
				return;
			}
			//进行词法分析，并同时组建DOM树
			lexer.startLexer();

			//将数据转移到domTree里，防止二次解析时数据重叠
			for (CommonTagNode commonTagNode : HtmlTagLevelStorage.getTagLevel(0))
			{
				domTree.getDomTree().add(commonTagNode);
			}
			
			//保存特殊节点的数据
			domTree.getSpecialMap().put("comment", lexer.getCommentArray());
			domTree.getSpecialMap().put("CDATA", lexer.getCDATAArray());
			domTree.getSpecialMap().put("doctype", lexer.getDoctypeArray());
			
			HtmlTagLevelStorage.getTagLevel(0).clear();
		}
		
		return;
	}
	
	//打开URL，获取输入流
	private BufferedReader openConnection()
	{
		try
		{
			if (url.startsWith("http"))	//include https
			{
				//在线网页
				URL resource = new URL(url);
				//TODO：文件编码的事情还要再看，暂时定为utf-8
				URLConnection connection = resource.openConnection();
				connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.65 Safari/537.36");
				InputStream inputStream = connection.getInputStream();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream, UTF8);
				return new BufferedReader(inputStreamReader);
			}
			else
			{
				//离线文件
				return new BufferedReader(new FileReader(url));
			}
		}
		catch (FileNotFoundException e)
		{
			logger.severe("Can not find the file : " + url);
			return null;
		}
		catch (MalformedURLException e)
		{
			logger.severe("The URL : " + url + " is malformed");
			return null;
		}
		catch (IOException e)
		{
			logger.severe("I/O exception occurs while reading : " + url);
			e.printStackTrace();
			return null;
		}
	}
	
	///==============================================================================
	//from DOM
	
	//符合filter的的元素
	public ArrayList<CommonTagElement> getElementByFilter(HtmlTagFilter tagFilter)
	{
		return domTree.getElementByFilter(tagFilter);
	}
	
	
	//符合单一标签名称的元素
	public ArrayList<CommonTagElement> getElementByTagName(String tagName)
	{
		return domTree.getElementByTagName(tagName);
	}
	
	//符合单一id的元素
	public ArrayList<CommonTagElement> getElementById(String idValue)
	{
		return domTree.getElementById(idValue);
	}
	
	
	//符合单一class的元素
	public ArrayList<CommonTagElement> getElementByClass(String classValue)
	{
		return domTree.getElementByClass(classValue);
	}
	
	//符合filter的的元素
	public ArrayList<CommonTagNode> getNodeByFilter(HtmlTagFilter tagFilter)
	{
		return domTree.getNodeByFilter(tagFilter);
	}
	
	
	//符合单一标签名称的元素
	public ArrayList<CommonTagNode> getNodeByTagName(String tagName)
	{
		return domTree.getNodeByTagName(tagName);
	}
	
	//符合单一id的元素
	public ArrayList<CommonTagNode> getNodeById(String idValue)
	{
		return domTree.getNodeById(idValue);
	}
	
	
	//符合单一class的元素
	public ArrayList<CommonTagNode> getNodeByClass(String classValue)
	{
		return domTree.getNodeByClass(classValue);
	}

	//获取指定标签元素集合中的正文，并做一定的格式控制
	public String getContent(ArrayList<CommonTagNode> tagNode)
	{
		return domTree.getContent(tagNode);
	}
	
	
	//获取特殊节点Map
	public HashMap<String, ArrayList<SpecialTagNode>> getSpecialMap()
	{
		return domTree.getSpecialMap();
	}
	
	//获取指定特殊节点ArrayList
	public ArrayList<SpecialTagNode> getSpecialTags(String key)
	{
		return domTree.getSpecialMap().get(key);
	}
	
	//============================================================================================================
	//get/set方法组
	
	//开启日志记录
	public void setEnableLogger(boolean status)
	{
		HtmlLogger.setEnableLogger(status);
	}
	
	//开启文件日志记录
	public void setEnableFileLogger(boolean status)
	{
		HtmlLogger.setEnableFileLogger(status);
	}
	
	//获取DOM对象
	public HtmlDomTree getDomTree()
	{
		return domTree;
	}
}
