package zust.java.jiangT.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import zust.java.jiangT.db.config.DbConfig;

/**
 * 
 * @ClassName: MySqlConnection 
 * @Description: 单例模式；这个类可以获得mysql连接
 * @author jiangT 
 * @date 2015年6月10日 下午8:26:03 
 *
 */
public final class MySqlConnection
{

	private static DbConfig config = DbConfig.getDbConfig();
	private static final String URL = config.getDbPropre("URL");
	private static final String USER = config.getDbPropre("USER");
	private static final String PASSWORD = config.getDbPropre("PASSWORD");
	private static Connection conn;
	static
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * 
	 * @Title: getConnection 
	 * @Description:  返回一个mysql连接
	 * @param @return
	 * @return Connection 
	 * @throws
	 */
	public static Connection getConnection()
	{
		return conn;
	}

	/**
	 * 
	 * @Title: closeConnection 
	 * @Description:  关闭mysql连接
	 * @param @return
	 * @return boolean 
	 * @throws
	 */
	public static boolean closeConnection()
	{
		boolean closed = false;
		if (conn != null)
		{
			try
			{
				conn.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
			finally
			{
				closed = true;
			}
		}
		return closed;
	}
}
