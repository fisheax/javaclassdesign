package zust.java.Any.view.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import zust.java.Any.view.Boundary;

public class EditorButtonListener implements ActionListener
{

	Boundary boundary;

	public EditorButtonListener(Boundary boundary)
	{
		this.boundary = boundary;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		Boundary.getContentArea().setEditable(true);
		Boundary.getLabel().setEditable(true);
		boundary.getKeepButton().setActionCommand("update");
	}

}
