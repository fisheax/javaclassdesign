package zust.java.lx.net.Exception;

public abstract class HtmlException extends Exception
{

	private static final long serialVersionUID = 641379814967131082L;
	
	public HtmlException()
	{
		
	}
	
	public HtmlException(String string)
	{
		super(string);
	}
	
	/*
	 * TODO：这里再看一下需不需要弄点什么
	 */
	public abstract void method();
}
