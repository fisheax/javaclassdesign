package zust.java.Any.view.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import zust.java.Any.view.Boundary;

public class CheckListerner implements ActionListener
{
	Boundary bou;

	public CheckListerner(Boundary boundary)
	{
		bou = boundary;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		bou.checkFavor();
		bou.getJMenuBar().getMenu(1).getItem(0).setEnabled(false);
		bou.getJMenuBar().getMenu(1).getItem(1).setEnabled(true);
	}

}
