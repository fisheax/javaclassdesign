package zust.java.Any.view.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import zust.java.Any.view.Boundary;

public class SelectListener implements ActionListener
{

	Boundary boundary;

	public SelectListener(Boundary boundary)
	{
		this.boundary = boundary;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		boundary.getKeepButton().setActionCommand("update");
		boundary.showView(e);
	}

}
