package zust.java.lx.net.TagElement;

/**
 * html文档特殊标签 comment + doctype +　CDATA + ...
 * @author muxue
 *
 */

public abstract class SpecialTagElement
{
	//标签名称
	private String tagName = null;
	//内容
	private String content = null;
	
	
	//SpecialTagElement的构造器
	public SpecialTagElement()
	{
		
	}
	
	public SpecialTagElement(String tagName)
	{
		this.tagName = tagName;
	}
	
	public SpecialTagElement(String tagName, String content)
	{
		this.tagName = tagName;
		this.content = content;
	}
	
	//=====================================================================================
	//get/set方法组
	
	//获取标签名称
	public String getTagName()
	{
		return tagName;
	}
	
	//获取标签内容
	public String getContent()
	{
		return content;
	}
}
