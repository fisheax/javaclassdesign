package zust.java.lx.net.TagElement;

/**
 * html的comment标签
 * @author muxue
 *
 */

public class CommentTagElement extends SpecialTagElement
{
	//CommentTagElement的构造器
	public CommentTagElement()
	{
		super("comment");
	}
	
	public CommentTagElement(String content)
	{
		super("comment", content);
	}
}
