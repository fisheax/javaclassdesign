/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : java_class

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-06-17 21:24:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for blog
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `blog_no` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(1024) NOT NULL,
  `body` varchar(12040) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `edit_date` datetime DEFAULT NULL,
  `comefrom` varchar(512) NOT NULL,
  PRIMARY KEY (`blog_no`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog
-- ----------------------------
INSERT INTO `blog` VALUES ('1', 'title', 'body\n', '2015-06-11 21:34:10', '2015-06-12 00:00:00', 'computer');
INSERT INTO `blog` VALUES ('120', '修改Mac下Safari设置无副作用轻松上Google\r         \r         修改Mac下Safari设置无副作用轻松上Google\r         \r         修改Mac下Safari设置无副作用轻松上Google', '在教育网的同学们可能遇到过很多这种情况，就是Google搜索搜着搜着，突然无法连接谷歌浏览器了，明明看到了搜索结果列表，但是一个搜索结果都无法访问，原因是你被ban掉了与谷歌的链接。不想买VPN 不想非常麻烦地翻墙，怎么办？\n\n    这里介绍一种方法修改Safari的设置，重定向谷歌搜索引擎。\n\n    1、安装Glimsglims是一款个性化配置浏览器的插件，有Safari版本。来到他的官网下载，http://machangout.com 安装。安装时会提示你安装基本版还是完整版。我选择的是基本版，等摸透了目前的功能 我们之后可以选择安装完整加强版。\n2、设置Glims in Safari打开Safari偏好设置 preference切换到glims标签我们首先需要打开增加搜索引擎的开关\n\n接下来新增一个搜索引擎点击“Add”，增加一个搜索引擎，名字可以随便取。http://203.208.46.148/search?q=#query#&ie=utf-8&oe=utf-8在query url一栏填写如下：此处使用了一个谷歌代理服务器IP地址，然后将query放在固定位置。点击“Set”加入搜索引擎列表。在Search Engine List里面\n在列表中刚刚创建的可能在比较下面，可以拖动到第一个。\n在Safari地址栏右侧 会出现选择搜索引擎的选项。\n\n选择我们刚刚创建的搜索引擎名称 之后就会用代理服务器上Google了，我再也没有无故断开过谷歌的链接。在教育网的同学们可能遇到过很多这种情况，就是Google搜索搜着搜着，突然无法连接谷歌浏览器了，明明看到了搜索结果列表，但是一个搜索结果都无法访问，原因是你被ban掉了与谷歌的链接。不想买VPN 不想非常麻烦地翻墙，怎么办？\n\n    这里介绍一种方法修改Safari的设置，重定向谷歌搜索引擎。\n\n    1、安装Glimsglims是一款个性化配置浏览器的插件，有Safari版本。来到他的官网下载，http://machangout.com 安装。安装时会提示你安装基本版还是完整版。我选择的是基本版，等摸透了目前的功能 我们之后可以选择安装完整加强版。\n2、设置Glims in Safari打开Safari偏好设置 preference切换到glims标签我们首先需要打开增加搜索引擎的开关\n\n接下来新增一个搜索引擎点击“Add”，增加一个搜索引擎，名字可以随便取。http://203.208.46.148/search?q=#query#&ie=utf-8&oe=utf-8在query url一栏填写如下：此处使用了一个谷歌代理服务器IP地址，然后将query放在固定位置。点击“Set”加入搜索引擎列表。在Search Engine List里面\n在列表中刚刚创建的可能在比较下面，可以拖动到第一个。\n在Safari地址栏右侧 会出现选择搜索引擎的选项。\n\n选择我们刚刚创建的搜索引擎名称 之后就会用代理服务器上Google了，我再也没有无故断开过谷歌的链接。ssssssssssssssssss', '2015-06-11 00:00:00', '2015-06-11 00:00:00', 'http://blog.csdn.net/rk2900/article/details/17355757');
INSERT INTO `blog` VALUES ('121', 'PHP开启异步多线程执行脚本\r         \r         Mac OS X开启网站Http服务与apache的方法（包括Moutain Lion）\r         \r         远程备份服务器自动获取备份via FTP\r         \r         服务器定时自动备份MySQL数据库\r         \r         MySQL增加只有记录操作权限无表权限的账户\r         \r         MySQL终端登录问题解决\r         \r         Nginx低权限账户配置的下载问题\r         \r         Nginx低权限账户配置\r         \r         整理一些关于SSH、FTP白名单设定的操作\r         \r         Nginx配置文件详细说明', '', '2015-06-11 00:00:00', '2015-06-11 00:00:00', 'http://blog.csdn.net/rk2900/article/category/1315702');
INSERT INTO `blog` VALUES ('122', '利用Naive Bayes分类器编写垃圾邮件过滤器\r         \r         Mac OS下导入NLTK到PyDev和Spyder', 'affffffffffffdvdcxv', '2015-06-11 00:00:00', '2015-06-11 00:00:00', 'http://blog.csdn.net/rk2900/article/category/1424541');
INSERT INTO `blog` VALUES ('123', '正则表达式来匹配文本串中的空白符', '/**\n	 * \n	 * <p>Title: test</p> \n	 * <p>Description: 用正则表达式来匹配文本串中的空白符，但是在正则表达式中加入了行的开头和行的结尾匹配符之后，匹配效果就不行了</p>  \n	 *\n	 */\n	public void test() {\n		// String regex = \"\\\\s+\";// 输出结果：我的测试文件.xml\n		String regex = \"^\\\\s+$\";// 输出结果：我的测试 文 件 .xml\n		String str = \"我的测试 文	件 .xml\";\n		String replaceAll = str.replaceAll(regex, \"\");\n		System.out.println(replaceAll);\n\n	}\n	\n	/**\n	 * \n	 * <p>\n	 * Title: removeSpacesFromFileName\n	 * </p>\n	 * <p>\n	 * Description: 将文件名中的空格删除\n	 * </p>\n	 * \n	 * @param filePath\n	 * @return\n	 *\n	 */\n	public static boolean removeSpacesFromFileName(String filePath) {\n		File file = new File(filePath);\n		String path = file.getPath();\n		path = path.replaceAll(\"\\\\s+\", \"\").replaceAll(\"　\",\"\");\n		return file.renameTo(new File(path));\n	}\n\n\n\n\n\nsfaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2015-06-11 00:00:00', '2015-06-11 00:00:00', '');
INSERT INTO `blog` VALUES ('124', '正则表达式来匹配文本串中的空白符', '/**\n	 * \n	 * <p>Title: test</p> \n	 * <p>Description: 用正则表达式来匹配文本串中的空白符，但是在正则表达式中加入了行的开头和行的结尾匹配符之后，匹配效果就不行了</p>  \n	 *\n	 */\n	public void test() {\n		// String regex = \"\\\\s+\";// 输出结果：我的测试文件.xml\n		String regex = \"^\\\\s+$\";// 输出结果：我的测试 文 件 .xml\n		String str = \"我的测试 文	件 .xml\";\n		String replaceAll = str.replaceAll(regex, \"\");\n		System.out.println(replaceAll);\n\n	}\n	ggggg\n	/**\n	 * \n	 * <p>\n	 * Title: removeSpacesFromFileName\n	 * </p>\n	 * <p>\n	 * Description: 将文件名中的空格删除\n	 * </p>\n	 * \n	 * @param filePath\n	 * @return\n	 *\n	 */\n	public static boolean removeSpacesFromFileName(String filePath) {\n		File file = new File(filePath);\n		String path = file.getPath();\n		path = path.replaceAll(\"\\\\s+\", \"\").replaceAll(\"　\",\"\");\n		return file.renameTo(new File(path));\n	}', '2015-06-11 00:00:00', '2015-06-11 00:00:00', '');
INSERT INTO `blog` VALUES ('125', '正则表达式来匹配文本串中的空白符', '/**\n	 * \n	 * <p>Title: test</p> \n	 * <p>Description: 用正则表达式来匹配文本串中的空白符，但是在正则表达式中加入了行的开头和行的结尾匹配符之后，匹配效果就不行了</p>  \n	 *\n	 */\n	public void test() {\n		// String regex = \"\\\\s+\";// 输出结果：我的测试文件.xml\n		String regex = \"^\\\\s+$\";// 输出结果：我的测试 文 件 .xml\n		String str = \"我的测试 文	件 .xml\";\n		String replaceAll = str.replaceAll(regex, \"\");\n		System.out.println(replaceAll);\n\n	}\n	\n	/**\n	 * \n	 * <p>\n	 * Title: removeSpacesFromFileName\n	 * </p>\n	 * <p>\n	 * Description: 将文件名中的空格删除\n	 * </p>\n	 * \n	 * @param filePath\n	 * @return\n	 *\n	 */\n	public static boolean removeSpacesFromFileName(String filePath) {\n		File file = new File(filePath);\n		String path = file.getPath();\n		path = path.replaceAll(\"\\\\s+\", \"\").replaceAll(\"　\",\"\");\n		return file.renameTo(new File(path));\n	}', '2015-06-11 00:00:00', '2015-06-11 00:00:00', '');
INSERT INTO `blog` VALUES ('126', 'OpenSceneGraph基础:Helloworld', 'OpenSceneGraph的基本流程（main函数）：转载请注明http://blog.csdn.net/boksic 如有疑问欢迎留言\n\n\n    \n\n    int main() {\n	Group *scene = startupScene();\n\n	osgViewer::Viewer viewer;\n	viewer.setSceneData(scene);\n	viewer.setCameraManipulator(new osgGA::TrackballManipulator);\n	viewer.realize();\n\n	while (!viewer.done()) {		\n		viewer.frame();\n		update(0.005); // do the update advancing 5ms\n	} \n\n	return 0;\n}\n\n\n    startupScene函数是自己建立的场景初始化函数，返回&#20540;是包含了场景图形数据的Group类接着初始化Viewer类，以及提供了模型操作功能的TrackballManipulator最后就是最为重要的渲染部分-Viewer的循环，每帧都会调用自定义的update函数\n\n    场景初始化（startupScene）建立模型数据这里使用的是程序内部初始化数据，包含了顶点坐标，面索引以及顶点颜色\n\n    （部分网上教程里还使用了顶点的颜色索引colorIndexArray，但该功能因为非常影响性能，现在的OSG中已废除了该功能。所以顶点与色彩应按对应关系初始化）\n\n    \n\n    	// 顶点\n	osg::Vec3Array *vertexArray = new osg::Vec3Array();\n	vertexArray->push_back(osg::Vec3(-2, -2, 0)); // front left \n	vertexArray->push_back(osg::Vec3(+2, -2, 0)); // front right \n	vertexArray->push_back(osg::Vec3(+2, +2, 0)); // back right \n	vertexArray->push_back(osg::Vec3(-2, +2, 0)); // back left \n	vertexArray->push_back(osg::Vec3(0, 0, 2*sqrt(2))); // peak\n	vertexArray->push_back(osg::Vec3(0, 0, -2*sqrt(2))); // lower peak\n\n	// 面，全部按逆时针顺序索引顶点\n	osg::DrawElementsUInt *faceArray = new osg::DrawElementsUInt(osg::PrimitiveSet::TRIANGLES, 0);\n	faceArray->push_back(0); // face 0\n	faceArray->push_back(1);\n	faceArray->push_back(4);\n	faceArray->push_back(1); // face 1\n	faceArray->push_back(2);\n	faceArray->push_back(4);\n	faceArray->push_back(2); // face 2\n	faceArray->push_back(3);\n	faceArray->push_back(4);\n	faceArray->push_back(3); // face 3\n	faceArray->push_back(0);\n	faceArray->push_back(4);\n	faceArray->push_back(0); // face 4\n	faceArray->push_back(5);\n	faceArray->push_back(1);\n	faceArray->push_back(2); // face 5\n	faceArray->push_back(1);\n	faceArray->push_back(5);\n	faceArray->push_back(3); // face 6\n	faceArray->push_back(2);\n	faceArray->push_back(5);\n	faceArray->push_back(0); // face 7\n	faceArray->push_back(3);\n	faceArray->push_back(5);\n\n	// 色彩\n	osg::Vec4Array *colorArray = new osg::Vec4Array();\n	colorArray->push_back(osg::Vec4(1.0f, 0.0f, 0.0f, 1.0f)); //index 0\n	colorArray->push_back(osg::Vec4(0.0f, 1.0f, 0.0f, 1.0f)); //index 1\n	colorArray->push_back(osg::Vec4(0.0f, 0.0f, 1.0f, 1.0f)); //index 2\n	colorArray->push_back(osg::Vec4(1.0f, 0.0f, 1.0f, 1.0f)); //index 3\n	colorArray->push_back(osg::Vec4(1.0f, 1.0f, 0.0f, 1.0f)); //index 4 \n	colorArray->push_back(osg::Vec4(1.0f, 1.0f, 0.5f, 1.0f)); //index 5\n\n\n    将建立的数据存为osg里的对象：\n\n    	osg::Geometry *geometry = new osg::Geometry();\n	geometry->setVertexArray(vertexArray);\n	geometry->setColorArray(colorArray);\n	geometry->setColorBinding(osg::Geometry::BIND_PER_VERTEX);\n	geometry->addPrimitiveSet(faceArray);\n\n\n\n    DrawPixels，封装opengl的glDrawPixels\nShapeDrawable，用于绘制OSG自带的简单集合体\nGeometry，功能最为广泛的子类，本例当中就是使用该类Geometry的setVertexArray() ， setColorArray() ， setNormalArray()等函数用于设置坐标，颜色，法线等数据。setColorBinding()用于设置颜色绑定方式，本例中的BIND_PER_VERTEX即每个顶点对应一个颜色。addPrimitiveSet()则告诉OSG如何利用这些数据绘制图形，前面我们使用了DrawElementsUInt为PrimitiveSe的派生类来按照三角形绘制数据，这点与OpenGL下的代码非常相&#20284;。\n\n    \n建立叶节点：\n\n    	Geode *pyramidObject = new Geode();\n	pyramidObject->addDrawable(geometry);\n\n\n\n    建立组节点\n\n    &#160;&#160;&#160; osg::Group *root = new osg::Group();\n\n&#160;&#160; &#160;for (int i = 0; i < 1000; i++) {\n&#160;&#160; &#160;&#160;&#160; &#160;tr[i] = new osg::PositionAttitudeTransform;\n&#160;&#160; &#160;&#160;&#160; &#160;tr[i]->setPosition(osg::Vec3(((float)rand() / RAND_MAX * 4) * 1000, ((float)rand() / RAND_MAX * 4) * 1000, 0));\n&#160;&#160; &#160;&#160;&#160; &#160;tr[i]->addChild(pyramidObject);\n\n&#160;&#160; &#160;&#160;&#160; &#160;root->addChild(tr[i]);\n&#160;&#160; &#160;}\n\n&#160;&#160; &#160;return root;\n}\n\n\n    Group有几个派生类，其中一个是Tranform类用于几何变换，不过Transform是虚基类，实际使用MatrixTransform或PositionAttitudeTransform这两个子类。本例中的tr是一个PositionAttitudeTransform类对象的数组，该类可以通过setPosition来设置位置或者通过四元数来实现旋转。\n\n    本程序建立了1000个实现随机分布的Transform子节点，每个节点都添加了前面的叶节点作为几何数据。\n\n    初始化的部分就结束了。\n\n    \n\n\n    场景更新\n\n    float t= 0;\nvoid update(float dt) {\n	// keep track of the time\n	t+= 0.02*dt;\n\n\n	double ro = (3 / 4)*(3.1415926 / 2);\n\n	//float myRand = ((float)rand() / RAND_MAX);\n\n	for (int i = 0; i < 1000; i++) {\n		osg::Vec3d currentPosition = tr[i]->getPosition();\n		osg::Vec3d *newPosition;\n\n\n \n			newPosition = new osg::Vec3d(sin(3 * t*i + ro) * (10 + i), sin(4 * t*i) ,i);//+ dt * (i + 1) / 10\n	  \n		tr[i]->setPosition(osg::Vec3(newPosition->x(), newPosition->y(), newPosition->z()));\n	}\n}ssssss\n更新部分通过全局变量t来控制时间，使用Transform节点的setPosition来控制模型位置，本例实现的是一个随时间变化的丽莎如图形：\n\n    \n\n    \n\n\n    \n\n\n    \n或者改变参数实现其他效果：', '2015-06-11 00:00:00', '2015-06-11 00:00:00', '');

-- ----------------------------
-- Table structure for favorites
-- ----------------------------
DROP TABLE IF EXISTS `favorites`;
CREATE TABLE `favorites` (
  `favor_no` int(11) NOT NULL AUTO_INCREMENT,
  `blog_no` int(11) DEFAULT NULL,
  `collection_date` datetime DEFAULT NULL,
  PRIMARY KEY (`favor_no`),
  KEY `FK_collection_blog` (`blog_no`),
  CONSTRAINT `FK_collection_blog` FOREIGN KEY (`blog_no`) REFERENCES `blog` (`blog_no`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of favorites
-- ----------------------------
INSERT INTO `favorites` VALUES ('10', '1', '2015-06-11 00:00:00');
INSERT INTO `favorites` VALUES ('13', '120', '2015-06-11 00:00:00');
INSERT INTO `favorites` VALUES ('14', '122', '2015-06-11 00:00:00');
