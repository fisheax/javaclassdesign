package zust.java.Any.view.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import zust.java.Any.view.Boundary;

public class DeleteListener implements ActionListener
{
	Boundary boundary;

	public DeleteListener(Boundary boundary)
	{
		this.boundary = boundary;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		boundary.deleteRow(e);
	}

}
