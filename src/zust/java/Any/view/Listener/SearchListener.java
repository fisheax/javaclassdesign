package zust.java.Any.view.Listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import zust.java.Any.view.Boundary;

public class SearchListener implements KeyListener
{

	Boundary boundary;

	public SearchListener(Boundary boundary)
	{
		this.boundary = boundary;
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		//拿不到第一个字符，因为这个事件还没有传递到JTextField上
		boundary.menesearch(e.getKeyChar());
	}

	@Override
	public void keyPressed(KeyEvent e)
	{
//		if (e.getKeyCode() == KeyEvent.VK_ENTER)
//		{
//			boundary.menesearch();
//		}
		
	}

	@Override
	public void keyReleased(KeyEvent e)
	{

	}

}