package zust.java.lx.net.DomTree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import zust.java.lx.net.Filter.HtmlClassFilter;
import zust.java.lx.net.Filter.HtmlIdFilter;
import zust.java.lx.net.Filter.HtmlTagFilter;
import zust.java.lx.net.Filter.HtmlTagNameFilter;
import zust.java.lx.net.TagElement.CommonTagElement;
import zust.java.lx.net.TagNode.CommonTagNode;
import zust.java.lx.net.TagNode.SpecialTagNode;
import zust.java.lx.net.Validator.TagNameValidator;

/**
 * 提供DOM操作的一切方法，DOM的解析结果是从lexer里来的
 * @author muxue
 *
 */

public class HtmlDomTree
{
	//
	private ArrayList<CommonTagNode> domTree = new ArrayList<CommonTagNode>();
	private HashMap<String, ArrayList<SpecialTagNode>> specialTags = new HashMap<String, ArrayList<SpecialTagNode>>();
	
	
	//
	public HtmlDomTree()
	{
		//Nothing should do
	}
	
	
	//==============================================================================
	//所有操作DOM的方法
	
	//===========================================================================================
	// About elements
	
	//符合filter的的元素
	public ArrayList<CommonTagElement> getElementByFilter(HtmlTagFilter tagFilter)
	{
		return traversalElements(tagFilter);
	}
	
	
	//符合单一标签名称的元素
	public ArrayList<CommonTagElement> getElementByTagName(String tagName)
	{
		if (!TagNameValidator.validateTagName(tagName))
		{
			return null;
		}
		return getElementByFilter(new HtmlTagNameFilter(tagName));
	}
	
	//符合单一id的元素
	public ArrayList<CommonTagElement> getElementById(String idValue)
	{
		return getElementByFilter(new HtmlIdFilter(idValue));
	}
	
	
	//符合单一class的元素
	public ArrayList<CommonTagElement> getElementByClass(String classValue)
	{
		return getElementByFilter(new HtmlClassFilter(classValue));
	}
	
	
	//===================================================================================
	
	//遍历DOM树元素入口
	private ArrayList<CommonTagElement> traversalElements(HtmlTagFilter htmlFilter)
	{
		ArrayList<CommonTagElement> gettedTagElements = new ArrayList<CommonTagElement>();
		
		for (CommonTagNode tagNode : domTree)
		{
			subTraversal(tagNode, gettedTagElements, htmlFilter);
		}
		
		return gettedTagElements;
	}
	
	
	//元素辅助子遍历
	private void subTraversal(CommonTagNode tagNode, ArrayList<CommonTagElement> gettedTagElements, HtmlTagFilter htmlFilter)
	{
		for (CommonTagNode childTagNode : tagNode.getChildNodes())
		{
			subTraversal(childTagNode, gettedTagElements, htmlFilter);
		}
		
		if (htmlFilter.filter(tagNode))
		{
			gettedTagElements.add(tagNode.getTagElement());
		}
		
		return;
	}
	
	//===========================================================================================
	// About nodes
	
	//符合filter的的节点
	public ArrayList<CommonTagNode> getNodeByFilter(HtmlTagFilter tagFilter)
	{
		return traversalNodes(tagFilter);
	}
	
	
	//符合单一标签名称的节点
	public ArrayList<CommonTagNode> getNodeByTagName(String tagName)
	{
		if (!TagNameValidator.validateTagName(tagName))
		{
			return null;
		}
		return getNodeByFilter(new HtmlTagNameFilter(tagName));
	}
	
	//符合单一id的节点
	public ArrayList<CommonTagNode> getNodeById(String idValue)
	{
		return getNodeByFilter(new HtmlIdFilter(idValue));
	}
	
	
	//符合单一class的节点
	public ArrayList<CommonTagNode> getNodeByClass(String classValue)
	{
		return getNodeByFilter(new HtmlClassFilter(classValue));
	}
	

	
	//遍历DOM树节点入口
	private ArrayList<CommonTagNode> traversalNodes(HtmlTagFilter htmlFilter)
	{
		ArrayList<CommonTagNode> gettedTagNodes = new ArrayList<CommonTagNode>();
		
		for (CommonTagNode tagNode : domTree)
		{
			subTraversalNode(tagNode, gettedTagNodes, htmlFilter);
		}
		
		return gettedTagNodes;
	}
	
	
	//节点辅助子遍历
	private void subTraversalNode(CommonTagNode tagNode, ArrayList<CommonTagNode> gettedTagNodes, HtmlTagFilter htmlFilter)
	{
		for (CommonTagNode childTagNode : tagNode.getChildNodes())
		{
			subTraversalNode(childTagNode, gettedTagNodes, htmlFilter);
		}
		
		if (htmlFilter.filter(tagNode))
		{
			gettedTagNodes.add(tagNode);
		}
		
		return;
	}
	
	//获取指定标签元素集合中的正文，并做一定的格式控制
	public String getContent(ArrayList<CommonTagNode> tagNodes)
	{
		ArrayList<CommonTagElement> tagElements = new ArrayList<CommonTagElement>();
		String content = null;
		
		for (CommonTagNode tagNode : tagNodes)
		{
			getSubElements(tagNode, tagElements);
			//同时应当加上自己节点上的数据
			tagElements.add(tagNode.getTagElement());
		}
		
		
		StringBuffer buffer = new StringBuffer();
		
		for (CommonTagElement tagElement : tagElements)
		{
			buffer.append(tagElement.getText());
			
			String tagName = tagElement.getTagName();
			
			if (tagName.equalsIgnoreCase("br"))
			{
				//在JTextArea里 \n 和 \r\n 的效果一致
				buffer.append("");
			}
			else if (tagName.equalsIgnoreCase("p")) 
			{
				buffer.append("\n  ");
			}
		}
		
		content = buffer.toString();
		
		for (Entry<String, String> symbol : HtmlSpecialSymbols.getSymbolTable().entrySet())
		{
			content = content.replaceAll(symbol.getKey(), symbol.getValue());
		}
		
		return content.trim();
	}
	
	
	//获取子元素
	private void getSubElements(CommonTagNode tagNode, ArrayList<CommonTagElement> tagElements)
	{
		for (CommonTagNode childTagNode : tagNode.getChildNodes())
		{
			getSubElements(childTagNode, tagElements);
			tagElements.add(childTagNode.getTagElement());
		}
		
		return;
	}


	//=====================================================================================
	//get/set 方法组
	
	//获取DOM树
	public ArrayList<CommonTagNode> getDomTree()
	{
		return domTree;
	}
	
	//获取特殊节点Map
	public HashMap<String, ArrayList<SpecialTagNode>> getSpecialMap()
	{
		return specialTags;
	}
	
	//获取指定特殊节点ArrayList
	public ArrayList<SpecialTagNode> getSpecialTags(String key)
	{
		return specialTags.get(key);
	}
}




