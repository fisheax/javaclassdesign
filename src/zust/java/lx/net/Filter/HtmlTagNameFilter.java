package zust.java.lx.net.Filter;

/**
 * 
 * 单一的标签名称过滤
 * @author muxue
 */

import zust.java.lx.net.TagNode.CommonTagNode;

public class HtmlTagNameFilter extends HtmlTagFilter
{
	
	//HtmlTagNameFilter的构造器
	public HtmlTagNameFilter(String tagName)
	{
		super(tagName);
	}

	
	@Override
	public boolean filter(CommonTagNode currentNode)
	{
		if (getSelectorText().toLowerCase().equals(currentNode.getTagName()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
