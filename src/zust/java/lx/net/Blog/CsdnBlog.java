package zust.java.lx.net.Blog;

import zust.java.lx.net.Filter.HtmlPChildFilter;



/**
 * csdn 的博客解析模板
 * @author muxue
 *
 */

public class CsdnBlog extends Blog
{
	
	//CsdnBlog构造器
	public CsdnBlog(String url)
	{
		super(url);
	}
	
	@Override
	public String getTitle()
	{
		return parser.getContent(parser.getNodeByFilter(new HtmlPChildFilter("div .article_title h1 span .link_title a")));
	}

	@Override
	public String getText()
	{
		return parser.getContent(parser.getNodeByFilter(new HtmlPChildFilter("div #article_content .article_content")));
	}

}
