package zust.java.lx.net.Filter;

/**
 * 
 * 单一的id过滤
 * @author muxue
 */

import zust.java.lx.net.TagNode.CommonTagNode;

public class HtmlClassFilter extends HtmlTagFilter
{

	//HtmlClassFilter的构造器
	public HtmlClassFilter(String tagName)
	{
		super(tagName);
	}

	
	@Override
	public boolean filter(CommonTagNode currentNode)
	{
		return getSelectorText().equalsIgnoreCase(currentNode.getClassValue());
	}
}
