package zust.java.lx.net.TagElement;

/**
 * html的doctype标签
 * @author muxue
 *
 */

public class DoctypeTagElement extends SpecialTagElement
{
	//DoctypeTagElement的构造器
	public DoctypeTagElement()
	{
		super("doctype");
	}
	
	public DoctypeTagElement(String content)
	{
		super("doctype", content);
	}
}
