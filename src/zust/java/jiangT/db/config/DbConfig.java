package zust.java.jiangT.db.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import zust.java.jiangT.db.mapping.Mapping;

public abstract class DbConfig
{

	private static DbConfig config;
	protected static Mapping mapping = Mapping.getMapping();
	private static Properties DbProperties;
	private static Properties classProperties;
	private static Map<String, String> dbProper = new HashMap<String, String>();

	/**
	 * 
	 * @Title: mappingConfig 
	 * @Description:  通过mapping.addMapping（）方法设置数据库表和数据模型的映射关系
	 * @param 
	 * @return void 
	 * @throws
	 */
	public abstract void mappingConfig();

	/**
	 * 
	 * @Title: putDbPropre 
	 * @Description:  提供数据库连接需要的参数map的get方法
	 * @param @param key
	 * @param @param value
	 * @return void 
	 * @throws
	 */
	private static void putDbPropre(String key, String value)
	{
		dbProper.put(key, value);
	}

	/**
	 * 
	 * @Title: getDbPropre 
	 * @Description:  通过key返回连接参数
	 * @param @param key
	 * @param @return
	 * @return String 
	 * @throws
	 */
	public String getDbPropre(String key)
	{
		return dbProper.get(key);
	}

	/**
	 * 
	 * @Title: init 
	 * @Description:  所有设置的初始化
	 * @param 
	 * @return void 
	 * @throws
	 */
	public static void init()
	{
		if (config == null)
		{
			System.out.println("DbConfig init...");
			try
			{
				getDbpropreties();
				String className = getClassPath();
				config = (DbConfig) Class.forName(className).newInstance();
				config.mappingConfig();
				mapping.init();
			}
			catch (Exception e)
			{
				e.printStackTrace();
				System.out.println("�����ļ�������");
			}

		}

	}

	/**
	 * 
	 * @Title: getClassPath 
	 * @Description:  读取设置类的配置路径
	 * @param @return
	 * @param @throws FileNotFoundException
	 * @param @throws IOException
	 * @return String 
	 * @throws
	 */
	private static String getClassPath() throws FileNotFoundException, IOException
	{
		File f = new File("classProperties.properties");
		FileInputStream fis = new FileInputStream(f);
		classProperties = new Properties();
		classProperties.load(fis);
		return classProperties.getProperty("classPath");
	}

	/**
	 * 
	 * @Title: getDbpropreties 
	 * @Description:  读取数据库配置文件
	 * @param @throws FileNotFoundException
	 * @param @throws IOException
	 * @return void 
	 * @throws
	 */
	private static void getDbpropreties() throws FileNotFoundException, IOException
	{
		File f = new File("dbProperties.properties");
		FileInputStream fis = new FileInputStream(f);
		DbProperties = new Properties();
		DbProperties.load(fis);
		putDbPropre("URL", DbProperties.getProperty("url"));
		putDbPropre("USER", DbProperties.getProperty("user"));
		putDbPropre("PASSWORD", DbProperties.getProperty("password"));
	}

	/**
	 * 
	 * @Title: getDbConfig 
	 * @Description:  返回一个DbConfig对象
	 * @param @return
	 * @return DbConfig 
	 * @throws
	 */
	public static DbConfig getDbConfig()
	{
		return config;
	}

}
