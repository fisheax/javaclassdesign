package zust.java.Any.view.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import zust.java.Any.view.Boundary;

public class GrabListener implements ActionListener
{

	Boundary boundary;

	public GrabListener(Boundary boundary)
	{
		this.boundary = boundary;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		boundary.grabMenuItemActionPerformed(e);
	}

}