package zust.java.lx.net.TagElement;

/**
 * html的CDATATagNode标签
 * @author muxue
 *
 */

public class CDATATagElement extends SpecialTagElement
{
	//CDATATagElement的构造器
	public CDATATagElement()
	{
		super("CDATA");
	}
	
	public CDATATagElement(String content)
	{
		super("CDATA", content);
	}
}
