package zust.java.Any.view.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import zust.java.Any.view.Boundary;

public class SaveAsListener implements ActionListener
{

	Boundary boundary;

	public SaveAsListener(Boundary boundary)
	{
		this.boundary = boundary;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		boundary.saveAsMenuItemActionPerformed(e);
	}

}