package zust.java.Any.view;

import zust.java.Any.view.bean.Blog;
import zust.java.Any.view.bean.Favorites;
import zust.java.jiangT.db.config.DbConfig;

public class Config extends DbConfig
{

	@Override
	public void mappingConfig()
	{

		mapping.addMapping("blog", Blog.class);
		mapping.addMapping("favorites", Favorites.class);
	}

}
