package zust.java.jiangT.db.mapping;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import zust.java.jiangT.db.bean.Bean;
import zust.java.jiangT.db.table.Table;

public class Mapping
{
	///////////////////单例////////////////////////////
	private static Mapping mapping;
	static
	{
		mapping = new Mapping();
	}

	public static Mapping getMapping()
	{
		return mapping;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private final Map<Class<? extends Bean>, Table> beanTableMapping = new HashMap<Class<? extends Bean>, Table>();
	private final Map<String, Class<? extends Bean>> beanToTableMapping = new HashMap<String, Class<? extends Bean>>();

	/**
	 * 
	 * @Title: addMapping 
	 * @Description: 向映射表中添加映射关系  
	 * @param @param tableName 数据库中的表名
	 * @param @param object 数据模型的class对象
	 * @return void 
	 * @throws
	 */
	public void addMapping(String tableName, Class<? extends Bean> object)
	{
		this.beanToTableMapping.put(tableName, object);
	}

	/**
	 * @Description : 根据映射表初始化Table模型和数据模型的class对象的映射关系
	 */
	public void init()
	{
		for (String tableName : beanToTableMapping.keySet())
		{
			Table table = new Table(tableName);
			try
			{
				table.init();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
				System.out.println("");
			}
			beanTableMapping.put(beanToTableMapping.get(tableName), table);
		}
	}

	/**
	 * 
	 * @Title: getMappedTable 
	 * @Description: 通过数据模型的class对象 返回与之映射的Table对象 
	 * @param @param object
	 * @param @return
	 * @return Table 
	 * @throws
	 */
	public Table getMappedTable(Class<?> object)
	{
		return beanTableMapping.get(object);
	}

	//	public Map<Class<? extends Bean<?>>, Table> getBeanTableMapping(String tableName){
	//		return beanTableMapping;
	//	}

	/**
	 * 
	 * @Title: getBeanClass 
	 * @Description:  通过数据库中表名返回与之映射的数据模型的class对象
	 * @param @param tableName
	 * @param @return
	 * @return Class<? extends Bean> 
	 * @throws
	 */
	public Class<? extends Bean> getBeanClass(String tableName)
	{
		return beanToTableMapping.get(tableName);
	}

	private Mapping()
	{

	}
}
