package zust.java.lx.net.DomTree;

import java.util.HashMap;

/**
 * html的特殊字符转换表
 * @author muxue
 *
 */

public class HtmlSpecialSymbols
{
	private static HashMap<String, String> symbols = new HashMap<String, String>();
	
	static
	{
		symbols.put("&nbsp;", " ");
		symbols.put("&lt;", "<");
		symbols.put("&gt;", ">");
		symbols.put("&amp;", "&");
		symbols.put("&quot;", "\"");
		symbols.put("&reg;", "®");
		symbols.put("&copy;", "©");
		symbols.put("&trade;", "™");
		symbols.put("&ensp;", " ");
		symbols.put("&emsp;", " ");
		symbols.put("&mdash;", "——");
		//and more ...
	}
	
	public static HashMap<String, String> getSymbolTable()
	{
		return symbols;
	}
}
