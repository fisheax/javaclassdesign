package zust.java.jiangT.db.bean;

import java.util.HashMap;
import java.util.Map;

import zust.java.jiangT.db.config.DbConfig;
import zust.java.jiangT.db.mapping.Mapping;
import zust.java.jiangT.db.table.Table;

/**
 * 
 * @ClassName: Bean 
 * @Description: 所有数据模型的抽象父类，提供setter和getter
 * @author jiangT 
 * @date 2015年6月10日 下午8:51:29 
 *
 */
public abstract class Bean
{
	// mapping中保存了数据库表和数据模型的映射
	Mapping mapping = Mapping.getMapping();
	//保存了数据模型对象实例的数据
	private Map<String, Object> attributes = new HashMap<String, Object>();

	/**
	 * 
	 * @Title: setAttribute 
	 * @Description:  通过键值对的形式为数据模型设置数据
	 * @param @param key 必须和数据库表中列的属性名相同
	 * @param @param data 必须和数据库表中列的数据类型一致
	 * @param @return
	 * @return boolean 
	 * @throws
	 */
	public boolean setAttribute(String key, Object data)
	{
		boolean succeed = false;
		Table table = mapping.getMappedTable(this.getClass());
		if (table.hasKey(key) != null)
		{
			attributes.put(key, data);
			succeed = true;
		}
		return succeed;
	}

	/**
	 * 
	 * @Title: getAttribute 
	 * @Description:  通过关键字获得模型中的的数据
	 * @param @param key 必须和数据库表中列的属性名相同
	 * @param @return
	 * @return Object 
	 * @throws
	 */
	public Object getAttribute(String key)
	{
		return attributes.get(key);
	}

	/**
	 * 
	 * @Title: getAttributeMap 
	 * @Description:返回保存了对象数据的map  
	 * @param @return 数据对象的attributes
	 * @return Map<String,Object> 
	 * @throws
	 */
	public Map<String, Object> getAttributeMap()
	{
		return attributes;
	}

	/**
	 * 
	 * @Title: updateAttribute 
	 * @Description:  用于 更新数据模型中一天数据记录的值
	 * @param @param key 必须和数据库表中列的属性名相同
	 * @param @param data 更新后的数据值
	 * @param @return
	 * @return boolean 
	 * @throws
	 */
	public boolean updateAttribute(String key, Object data)
	{
		boolean succeed = false;
		if (checkAttribute(key))
		{
			attributes.remove(key);
			attributes.put(key, data);
			succeed = true;
		}
		return succeed;
	}

	/**
	 * 
	 * @Title: checkAttribute 
	 * @Description:  检测该模型中是否含有关键字是key的数据值
	 * @param @param key
	 * @param @return 如果存在返回true
	 * @return boolean 
	 * @throws
	 */
	public boolean checkAttribute(String key)
	{
		boolean isExist = false;
		if (attributes.get(key) != null)
		{
			isExist = true;
		}
		return isExist;
	}

	/**
	 * 
	 * @Title: removeAttribute 
	 * @Description:  通过关键字key 删除数据对象中与key对应的数据值
	 * @param @param key
	 * @param @return
	 * @return boolean 
	 * @throws
	 */
	public boolean removeAttribute(String key)
	{
		boolean succeed = false;
		if (checkAttribute(key))
		{
			attributes.remove(key);
			succeed = true;
		}
		return succeed;
	}

	/////////////////////////////////////////////////////////////////////

	/**
	 * 在创建数据模型的时候完成初始化
	 * @param 
	 *
	 */
	public Bean()
	{
		DbConfig.init();
	}
}
