package zust.java.lx.net.Lexer;

/**
 * html词法分析过程中的状态
 * @author muxue
 *
 */

public enum HtmlTagStatus
{
	/*
	 * 对应HtmlStatus的各种状态
	 */
	
	//data
	ReadData("ReadData"),
	//tag open
	TagOpen("TagOpen"),
	//end tag open
	TagEnd("TagEnd"),
	//tag close
	TagClosed("TagClosed"),
	//markup declaration open
	MarkTagOpen("MarkTagOpen"),
	//tag name
	TagName("TagName"),
	//self closing
	TagSelfClose("TagSelfClose"),
	//before attribute name
	//BeforeAttrName("BeforeAttrName"),
	//attribute name
	AttrName("AttrName"),
	//before attribute value
	//BeforeAttrValue("BeforeAttrValue"),
	//attribute value
	AttrValue("AtrrValue"),
	//all finished
	AllFinished("AllFinished"),
	//tag invalid
	TagInvalid("TagInvalid");

	private String name = null;
	
	private HtmlTagStatus(String name)
	{
		this.name = name;
	}

	
	//==================================================================
	//get/set方法组
	
	
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
