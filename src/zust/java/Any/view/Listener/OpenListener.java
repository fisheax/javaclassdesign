package zust.java.Any.view.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import zust.java.Any.view.Boundary;

public class OpenListener implements ActionListener
{

	Boundary boundary;

	public OpenListener(Boundary boundary)
	{
		this.boundary = boundary;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		boundary.openMenuItemActionPerformed(e);
	}

}
