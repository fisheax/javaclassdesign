package zust.java.lx.net.TagElement;

/**
 * html文档一般标签， 标签名称， 属性和正文
 * @author muxue
 */

import java.util.HashMap;

public class CommonTagElement
{
	//标签名称
	private String tagName = null;
	//属性
	private HashMap<String, String> attrs = new HashMap<String, String>();
	//正文
	private String text = null;
	
	
	//CommonTagElement的构造器
	public CommonTagElement()
	{
		
	}
	
	public CommonTagElement(String tagName)
	{
		this.tagName = tagName;
	}
	
	public CommonTagElement(String tagName, String text)
	{
		this.tagName = tagName;
		this.text = text;
	}
	
	public CommonTagElement(String tagName, HashMap<String, String> attrs, String text)
	{
		this.tagName = tagName;
		this.attrs = attrs;
		this.text = text;
	}

	//==================================================================================
	//get/set方法组
	
	
	public String getTagName()
	{
		return tagName;
	}

	public void setTagName(String tagName)
	{
		this.tagName = tagName;
	}

	public HashMap<String, String> getAttrs()
	{
		return attrs;
	}

	public void setAttrs(HashMap<String, String> attrs)
	{
		this.attrs = attrs;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}
	
	//
}
