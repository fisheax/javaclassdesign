package zust.java.Any.view.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

public class MenuTable extends AbstractTableModel
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Map<String, Blog> menuMapTable = new HashMap<String, Blog>();
	private static ArrayList<Blog> blogTable = new ArrayList<Blog>();

	public static Blog getBlog(String key)
	{
		return menuMapTable.get(key);
	}

	public static void putmenuMapTable(String key, Blog blog)
	{
		menuMapTable.put(key, blog);
	}

	public static Map<String, Blog> getMenuMapTable()
	{
		return menuMapTable;
	}

	public static void setBlogTable(ArrayList<Blog> bTable)
	{
		blogTable = bTable;
	}

	public static ArrayList<Blog> getBlogTable()
	{
		return blogTable;
	}

	public static void addBlogTable(Blog b)
	{
		blogTable.add(b);
	}

	public static void setMeunMapping()
	{
		for (Blog bl : blogTable)
		{
			putmenuMapTable(bl.getAttribute("blog_no") + "_" + bl.getAttribute("title"), bl);
		}
	}

	@Override
	public int getRowCount()
	{
		return blogTable.size();
	}

	@Override
	public int getColumnCount()
	{
		return 1;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		Blog blog = blogTable.get(rowIndex);
		String tit = (String) blog.getAttribute("title");
		String id = blog.getAttribute("blog_no") + "";
		return id + "_" + tit;
	}

	/**
	 * 在界面上设置表格的列名
	 */
	@Override
	public String getColumnName(int arg0)
	{
		return "目录";
	}
}
