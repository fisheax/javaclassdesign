package zust.java.Any.view;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class MenuPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	private JTable jTable = new JTable();
	JTextField searchTextField = new JTextField();

	public JTextField getSearchTextField()
	{
		return searchTextField;
	}

	public MenuPanel()
	{
		setLayout(new BorderLayout());
		JScrollPane itemPane = new JScrollPane(jTable);
		add(searchTextField, BorderLayout.NORTH);
		itemPane.setPreferredSize(new Dimension(130, 430));
		add(itemPane, BorderLayout.SOUTH);
	}

	public JTable geTable()
	{
		return jTable;
	}
}
