package zust.java.Any.view.Listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import zust.java.Any.view.Boundary;

public class SaveListener implements ActionListener
{

	Boundary boundary;

	public SaveListener(Boundary boundary)
	{
		this.boundary = boundary;
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		boundary.saveMenuItemActionPerformed(e);
	}

}
