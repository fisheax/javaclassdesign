package zust.java.lx.net.TagNode;

import zust.java.lx.net.TagElement.SpecialTagElement;

/**
 * doctype comment CDATA 标签节点
 * @author muxue
 *
 */

public class SpecialTagNode
{
	//标签元素
	private SpecialTagElement tagElement = null;
	//标签名称
	private String tagName = null;
	
	
	//SpecialTagNode的构造器
	public SpecialTagNode()
	{
		//No use, only for test
	}
	
	public SpecialTagNode(SpecialTagElement tagElement)
	{
		this.tagElement = tagElement;
		tagName = tagElement.getTagName();
	}
	
	//===============================================================
	//get/set方法组
	
	//获取标签元素
	public SpecialTagElement getTagElement()
	{
		return tagElement;
	}
	
	
	//获取标签名称
	public String getTagName()
	{
		return tagName;
	}
}
