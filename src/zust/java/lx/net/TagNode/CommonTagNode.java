package zust.java.lx.net.TagNode;

import java.util.ArrayList;

import zust.java.lx.net.TagElement.CommonTagElement;

/**
 * 普通标签节点
 * @author muxue
 *
 */

public class CommonTagNode
{
	//标签元素
	private CommonTagElement tagElement = null;
	//父节点
	private CommonTagNode parentNode = null;
	//子节点组
	private ArrayList<CommonTagNode> childNodes = new ArrayList<CommonTagNode>();
	//左兄弟节点
	private CommonTagNode leftBrotherNode = null;
	//右兄弟节点
	private CommonTagNode rightBrotherNode = null;
	
	
	//标签名称
	private String tagName = null;
	//标签id属性
	private String idValue = null;
	//标签class属性
	private String classValue = null;
	
	
	
	//CommonTagNode的构造器
	public CommonTagNode()
	{
		//No use, only for test
	}
	
	public CommonTagNode(CommonTagElement tagElement)
	{
		this.tagElement = tagElement;
		tagName = tagElement.getTagName();
		idValue = tagElement.getAttrs().get("id");
		classValue = tagElement.getAttrs().get("class");
	}
	
	
	@Override
	public String toString()
	{
		return "Name:\t" + tagName + ",\t\t\tid:\t" + idValue + ",\t\t\t\tclass:\t" + classValue;
	}
	
	//===============================================================
	//get/set方法组
	
	//取标签名称
	public String getTagName()
	{
		return tagName;
	}
	
	//取标签元素
	public CommonTagElement getTagElement()
	{
		return tagElement;
	}
	
	
	//取标签元素父节点
	public CommonTagNode getParentNode()
	{
		return parentNode;
	}
	
	
	//取标签子节点组
	public ArrayList<CommonTagNode> getChildNodes()
	{
		return childNodes;
	}
	
	//取标签子节点
	public CommonTagNode getChildNode(int index)
	{
		return childNodes.get(index);
	}
	
	//取标签左兄弟节点
	public CommonTagNode getLeftBrotherNode()
	{
		return leftBrotherNode;
	}
	
	
	//取标签右兄弟节点
	public CommonTagNode getRightBrotherNode()
	{
		return rightBrotherNode;
	}

	//设置父节点
	public void setParentNode(CommonTagNode parentNode)
	{
		this.parentNode = parentNode;
	}

	
	//设置左兄弟节点
	public void setLeftBrotherNode(CommonTagNode leftBrotherNode)
	{
		this.leftBrotherNode = leftBrotherNode;
	}

	
	//设置右兄弟节点
	public void setRightBrotherNode(CommonTagNode rightBrotherNode)
	{
		this.rightBrotherNode = rightBrotherNode;
	}

	//获取标签id属性值
	public String getIdValue()
	{
		return idValue;
	}
	
	
	//获取标签class属性值
	public String getClassValue()
	{
		return classValue;
	}
	
	
	//获取标签id属性值
	public void setIdValue(String idValue)
	{
		this.idValue = idValue;
	}
	
	
	//获取标签class属性值
	public void setClassValue(String classValue)
	{
		this.classValue = classValue;
	}
}
