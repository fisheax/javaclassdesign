package zust.java.lx.net.Filter;

/**
 * 
 * 单一的id过滤
 * @author muxue
 */

import zust.java.lx.net.TagNode.CommonTagNode;

public class HtmlIdFilter extends HtmlTagFilter
{

	//HtmlIdFilter的构造器
	public HtmlIdFilter(String tagName)
	{
		super(tagName);
	}

	
	@Override
	public boolean filter(CommonTagNode currentNode)
	{
		if (getSelectorText().toLowerCase().equals(currentNode.getIdValue()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
