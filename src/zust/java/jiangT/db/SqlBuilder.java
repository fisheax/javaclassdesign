package zust.java.jiangT.db;

import java.util.ArrayList;
import java.util.Map.Entry;

import zust.java.jiangT.db.bean.Bean;
import zust.java.jiangT.db.table.Table;

/**
 * 
 * @ClassName: SqlBuilder 
 * @Description: 创建用于增删改查的sql命令
 * @author jiangT 
 * @date 2015年6月10日 下午8:45:33 
 *
 */
public class SqlBuilder
{
	//保存？所对应的参数  第一个？对应下标为0的元素
	private static ArrayList<Object> paras;

	/**
	 * 
	 * @Title: getPapras 
	 * @Description:  返回保存了参数的ArrayList
	 * @param @return
	 * @return ArrayList<Object> 
	 * @throws
	 */
	public static ArrayList<Object> getPapras()
	{
		return paras;
	}

	/**
	 * 
	 * @Title: SqlForInsert 
	 * @Description: 创建用于插入的sql命令 
	 * @param @param table
	 * @param @param object
	 * @param @return
	 * @return String 
	 * @throws
	 */
	public String SqlForInsert(Table table, Bean object)
	{
		paras = new ArrayList<Object>();
		String sql = "insert into " + table.getName() + " (";
		String temp = ")values(";
		int i = 1;
		int len = object.getAttributeMap().size();
		for (Entry<String, Object> e : object.getAttributeMap().entrySet())
		{

			String columnName = e.getKey();
			if (i++ < len)
			{
				sql += columnName + ",";
				temp += "?,";
			}
			else
			{
				sql += columnName + " ";
				temp += "?);";
			}
			paras.add(e.getValue());
		}
		sql += temp;
		System.out.println(sql);
		return sql;
	}

	/**
	 * 
	 * @Title: SqlForUptate 
	 * @Description:  创建用于更新的sql
	 * @param @param table
	 * @param @param object
	 * @param @return
	 * @return String 
	 * @throws
	 */
	public String SqlForUptate(Table table, Bean object)
	{
		paras = new ArrayList<Object>();
		String sql = "update " + table.getName() + " set ";
		Object primaryKeyValue = null;
		String columnName = null;
		//int i=1;
		//int len=object.getAttributeMap().size();
		for (Entry<String, Object> e : object.getAttributeMap().entrySet())
		{
			columnName = e.getKey();
			if (columnName.equals(table.getPrimaryKey()))
			{
				primaryKeyValue = e.getValue();
			}
			else
			{
				//if (i<len) {
				sql += columnName + "=?,";
				//}else{
				//	sql+=columnName+"=? ";
				//}
				paras.add(e.getValue());
			}
			//System.out.println(columnName+"\\"+(i++));;
		}
		if (sql.endsWith(","))
		{
			sql = sql.substring(0, sql.length() - 1);
		}
		sql += " where " + table.getPrimaryKey() + "=" + primaryKeyValue.toString() + ";";
		System.out.println(sql);
		return sql;
	}

	/**
	 * 
	 * @Title: SqlForSelect 
	 * @Description:  创建用于查找的sql命令
	 * @param @param table
	 * @param @param object
	 * @param @return
	 * @return String 
	 * @throws
	 */
	public String SqlForSelect(Table table, Bean object)
	{
		String sql = "select * from " + table.getName() + " where ";
		for (Entry<String, Object> e : object.getAttributeMap().entrySet())
		{
			String columnName = e.getKey();
			if (columnName.equals(table.getPrimaryKey()))
			{
				sql += columnName + "=" + e.getValue() + ";";
			}
		}
		System.out.println(sql);
		return sql;
	}

	/**
	 * 
	 * @Title: SqlForDelete 
	 * @Description:  创建用于删除的sql命令
	 * @param @param table
	 * @param @param object
	 * @param @return
	 * @return String 
	 * @throws
	 */
	public String SqlForDelete(Table table, Bean object)
	{
		String sql = "delete from " + table.getName() + " where ";
		for (Entry<String, Object> e : object.getAttributeMap().entrySet())
		{
			String columnName = e.getKey();
			if (columnName.equals(table.getPrimaryKey()))
			{
				sql += columnName + "=" + e.getValue() + ";";
			}
		}
		System.out.println(sql);
		return sql;
	}
}
